import os
import sys
import urllib.request, urllib.parse, urllib.error
from packages.shp import mysql_schema

copy_shp="""
mkdir -p /opt/python/;
cp -Rn packages/* /opt/python/;
"""

def setupServer():
	print("#### Server Setup ####")
	ps="> Server Setup > "
	print("MySql Username and Password")
	db_username=str(input(ps+"Username: "))
	db_password=str(input(ps+"Password: "))
	print("Copy files")
	os.popen(copy_shp)
	print("Create Database")
	mysql_schema.createShpServer(db_username,db_password)
	ebene2=True
	while ebene2:
		print("Create Smart Home Pi Owner")
		username=str(input(ps+"Username: "))
		password=str(input(ps+"Password: "))
		password2=str(input(ps+"Type password again: "))
		if password == password2:
			mysql_schema.createOwner(db_username,db_password,username,password)
			ebene2=False
		else:
			print('"Password" and "Type password again" must be the same.')
	print("Finish")
	return True

def setupClient():
	print("#### Client Setup ####")
	ps="> Client Setup > "
	print("MySql Username and Password")
	db_username=str(input(ps+"Username: "))
	db_password=str(input(ps+"Password: "))
	print("Copy files")
	#nur wenn es nicht schon existiert
	os.popen(copy_shp)
	print("Create Database")
	mysql_schema.createShpClient(db_username,db_password)
	
	import sys
	sys.path.append('/opt/python/')
	from shp.ClientApp import ClientApp
	
	client_app=ClientApp(DB_USERNAME=db_username,DB_PASSWORD=db_password)
	
	ebene2=True
	while ebene2:
		print("Type in the ip adress of the server and the login data")
		ip_adress=str(input(ps+"Ip-Address: "))
		#TODO
		params={}
		params["username"]=str(input(ps+"Username: "))
		params["password"]=str(input(ps+"Password: "))
		print("Where is your client?")
		params["place"]=str(input(ps+"Place: "))
		try:
			details = urllib.parse.urlencode(params)
			binary_details=details.encode('utf8')
			url = urllib.request.Request('http://'+ip_adress+':5522/clients/add/', binary_details)
			url.add_header("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.29 Safari/525.13")
			responseData = urllib.request.urlopen(url).read().decode('utf8', 'ignore')
			responseFail = False

		except urllib.error.HTTPError as e:
			responseData = e.read().decode('utf8', 'ignore')
			responseFail = False

		except urllib.error.URLError:
			responseFail = True

		except socket.error:
			responseFail = True

		except socket.timeout:
			responseFail = True

		except UnicodeEncodeError:
			print("[x]  Encoding Error")
			responseFail = True
		if not responseFail:
			if not "False" in responseData:
				key=responseData
				#save key
				validation=client_app.servctrl.createItem(ip_adress=ip_adress,key=key)
				if validation:
					return True
			else:
				print("Response: "+str(responseData))
		else:
			print("Die Verbindung zum Server ist fehlgeschlagen")

def main():
	msg="""Smart Home Pi Setup:

  1. Setup new Server
  2. Setup new Client

  0. Exit
"""
	ebene1=True
	while ebene1:
		ps="> "
		print(msg)
		option=input(ps)
		try:
			option=int(option)
			if option==1:
				validation=setupServer()
				if validation:
					return True
			if option==2:
				validation=setupClient()
				if validation:
					return True
			if option==0:
				return True
		except ValueError:
			print("Type a number!")

if __name__=="__main__":
	main()
