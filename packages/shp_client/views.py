from flask import render_template, request, redirect, url_for, session
from __main__ import app
import sys
sys.path.append('/opt/python/')
from shp.ClientApp import ClientApp

#from config.conf read DB_USERNAME and DB_PASSWORD
app.config.from_object('config')
client_app=ClientApp(DB_USERNAME=app.config["DB_USERNAME"],DB_PASSWORD=app.config["DB_PASSWORD"],SEND_CMD=app.config["SEND_CMD"])

@app.route('/')
def index():
	server=client_app.servctrl.getAllItems()
	return render_template('index.html', server=server)

@app.route('/send', methods=['GET','POST'])
def send():
	"""
	 @author: marco schlicht
	 @co-author: kai rarog
	 
	 @key:str
	 @dev_code:int
	 @sec_code:int
	 @status:boolean
	"""
	validation=False
	params={}
	if request.method == 'POST':
		if "key" in request.form.keys() and "dev_code" in request.form.keys() and "sec_code" in request.form.keys() and "status" in request.form.keys():
			#try:
				params["key"]=str(request.form['key'])
				params["dev_code"]=int(request.form['dev_code'])
				params["sec_code"]=str(request.form['sec_code'])
				#vllt können hier probleme auftreten
				params["status"]=int(request.form['status'])
				params["ip_server"]=request.remote_addr
				validation=client_app.send(**params)
			#except:
			#	validation=False
	return str(validation)
