from flask import Flask
app = Flask(__name__)
import views

if __name__=="__main__":
	#port 5522 = hälfte von 11044
	#summe der html codes für die unicode zeichen von ❤ (10084) und π (pi) (960) ist 11044 und es ergibt das Zeichen von einem Kreis
	#π + ❤ = ⬤
	#fanden wir cool
	app.run(host="0.0.0.0",port=5522)
