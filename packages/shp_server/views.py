from flask import render_template, flash, request, redirect, url_for, session
from __main__ import app
import sys
sys.path.append('/opt/python/')
from shp.ServerApp import ServerApp

app.config.from_object('config')
server_app=ServerApp(DB_USERNAME=app.config["DB_USERNAME"],DB_PASSWORD=app.config["DB_PASSWORD"])

def check_login():
	if not "logged_in" in session.keys():
		return False
	else:
		if session["logged_in"]:
			return True
		else:
			return False

@app.route('/')
def index():
	devices=server_app.devctrl.getAllItems()
	return render_template('devices.html', server_app=server_app, session=session, devices=devices)

"""
DEVICES
"""
@app.route('/devices/')
def devices():
	try:
		devices=server_app.devctrl.getAllItems()
		return render_template('devices.html', server_app=server_app, session=session, devices=devices)
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('index'))

@app.route('/devices/add/', methods=['GET','POST'])
def add_device():
	try:
		perm="device.createDev"
		perm_id=10
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		if not server_app.usrctrl.selfHasPermission(session["ssid"], perm_id):
			flash("You are not allowed to create new devices!","warning")
			return redirect(url_for('devices'))
		if request.method == 'POST':
			errors=0
			if "description" in request.form.keys() and\
			   "sec_code" in request.form.keys() and\
			   "dev_code" in request.form.keys():
				if request.form["description"] and\
				   request.form["sec_code"] and\
				   request.form["dev_code"]:
					description=str(request.form["description"])
					sec_code=str(request.form["sec_code"])
					dev_code=int(request.form["dev_code"])
					place=str(request.form["place"])
					validation=server_app.devctrl.createItem(session["ssid"],description=description,sec_code=sec_code,dev_code=dev_code,place=place)
					if validation:
						flash("You successfully created the new device with ID %s"%(validation),"success")
						return redirect(url_for('devices'))
					else:
						flash(validation,"warning")
				else:
					if not request.form["description"]:
						flash("You have to insert a name","warning")
					if not request.form["sec_code"]:
						flash("You have to insert a security code","warning")
					if not request.form["dev_code"]:
						flash("You have to insert a device code","warning")
			else:
				flash("You have to insert a name, a security code and a device code","warning")
		return render_template('add_device.html', server_app=server_app, session=session)
	except Exception as e:
		flash(str(e), 'danger')
		return render_template('add_device.html', server_app=server_app, session=session)
	
@app.route('/devices/mydevices/')
def mydevices():
	try:
		devices=server_app.devctrl.getAllItems()
		if not check_login():
			flash('You must login to do that.', 'warning')
		eingeloggter_user=server_app.usrctrl.getEingeloggterUser(session["ssid"])
		new_devices=[]
		for device in devices:
			if device.getUser() == eingeloggter_user:
				new_devices.append(device)
		devices=new_devices
		return render_template('devices.html', server_app=server_app, session=session, devices=devices)
	
	except Exception as e:
		flash(str(e), 'danger')
		return render_template('devices.html', server_app=server_app, session=session, devices=devices)
"""
#wenn device groups existieren:
@app.route('/devices/filter/<int:id_dev_grp>/')
def filterdevices(id_dev_group):
	try:
		if request.method == 'GET':
			if "mydevices" in request.args.keys():
				if not check_login():
					flash('You must login to do that.', 'warning')
				eingeloggter_user=server_app.getEingeloggterUser(session["ssid"])
				new_devices=[]
				if request.args["mydevices"]==1:
					for device in devices:
						if device.getId() == eingeloggter_user:
							new_devices.append(device)
				else:
					for device in devices:
						if not device.getId() == eingeloggter_user:
							new_devices.append(device)
				devices=new_devices
		devices=server_app.devctrl.getAllItems()
		return render_template('devices.html', server_app=server_app, session=session, devices=devices)
	
	except Exception as e:
		flash(str(e), 'danger')
		return render_template('devices.html', server_app=server_app, session=session, devices=devices)
"""

@app.route('/devices/<int:dev_id>/')
def show_device(dev_id):
	try:
		dev_id=int(dev_id)
		device=server_app.devctrl.getItemById(dev_id)
		if device:
			return render_template('show_device.html', server_app=server_app, session=session, device=device)
		else:
			return redirect(url_for('devices'))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))

@app.route('/devices/<int:dev_id>/edit/', methods=['GET','POST'])
def edit_device(dev_id):
	try:
		dev_id=int(dev_id)
		device=server_app.devctrl.getItemById(int(dev_id))
		if device:
			if request.method == 'POST':
				if not check_login():
					flash('You must login to do that.', 'warning')
					return redirect(url_for('show_device', dev_id=dev_id))
				ssid=session["ssid"]
				if "description" in request.form.keys():
					description=str(request.form['description'])
					if device.getDescription() != description:
						if validation:
							validation=server_app.devctrl.setDescription(ssid,dev_id,description)
							flash("You successfully updated the description of device %s"%(dev_id),"success")
						else:
							flash("The update of the description on device %s failed"%(dev_id),"warning")
				if "sec_code" in request.form.keys():
					sec_code=str(request.form['sec_code'])
					if device.getSecCode() != sec_code:
						validation=server_app.devctrl.setSecCode(ssid,dev_id,sec_code)
						if validation:
							flash("You successfully updated the security code of device %s"%(dev_id),"success")
						else:
							flash("The update of the security code on device %s failed"%(dev_id),"warning")
				if "dev_code" in request.form.keys():
					dev_code=int(request.form['dev_code'])
					if device.getDevCode() != dev_code:
						validation=server_app.devctrl.setDevCode(ssid,dev_id,dev_code)
						if validation:
							flash("You successfully updated the device code of device %s"%(dev_id),"success")
						else:
							flash("The update of the device code on device %s failed"%(dev_id),"warning")
				if "place" in request.form.keys():
					place=str(request.form['place'])
					if device.getPlace() != place:
						validation=server_app.devctrl.setPlace(ssid,dev_id,place)
						if validation:
							flash("You successfully updated the device code of device %s"%(dev_id),"success")
						else:
							flash("The update of the device code on device %s failed"%(dev_id),"warning")
				return redirect(url_for('show_device', dev_id=dev_id))
			return render_template('edit_device.html', server_app=server_app, session=session, device=device)
		else:
			flash("Es gibt dieses Gerät nicht","warning")
			return redirect(url_for('devices'))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('show_device', dev_id=dev_id))

@app.route('/devices/<int:dev_id>/delete/', methods=['GET','POST'])
def del_device(dev_id):
	try:
		dev_id=int(dev_id)
		device=server_app.devctrl.getItemById(int(dev_id))
		if device:
			if not check_login():
				flash('You must login to do that.', 'warning')
				return redirect(url_for('show_device', dev_id=dev_id))
			ssid=session["ssid"]
			dev_name=device.getDescription()
			validation=server_app.devctrl.delItem(ssid,dev_id)
			if validation:
				flash("Sie haben erfolgreich das Gerät %s gelöscht"%(dev_name),"success")
				return redirect(url_for('devices'))
			else:
				flash("Es hat leider nicht geklappt","warning")
				return redirect(url_for('show_device', dev_id=dev_id))
		else:
			flash("Es gibt dieses Gerät nicht","warning")
			return redirect(url_for('devices'))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('show_device', dev_id=dev_id))

@app.route('/devices/<int:dev_id>/on/')
def turn_on(dev_id):
	try:
		dev_id=int(dev_id)
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		else:
			ssid=session["ssid"]
			validation=server_app.devctrl.setStatus(ssid,dev_id,1)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('devices'))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))

@app.route('/devices/<int:dev_id>/off/')
def turn_off(dev_id):
	try:
		dev_id=int(dev_id)
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		else:
			ssid=session["ssid"]
			validation=server_app.devctrl.setStatus(ssid,dev_id,0)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('devices'))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))
	
"""
BWL
"""
@app.route('/devices/<int:dev_id>/bwl/')
def show_bwl(dev_id):
	try:
		dev_id=int(dev_id)
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		else:
			ssid=session["ssid"]	
			dev_id=int(dev_id)
			device=server_app.devctrl.getItemById(dev_id)
			if not server_app.devctrl.isAllowedToManageBwl(ssid,dev_id):
				flash("You are not allowed to do this","warning")
				return redirect(url_for('devices'))
			if device:
				blacklisted_users=server_app.devctrl.getBlacklistedUsers(ssid,device.getId())
				whitelisted_users=server_app.devctrl.getWhitelistedUsers(ssid,device.getId())
				return render_template('show_bwl.html', server_app=server_app, session=session, device=device, blacklisted_users=blacklisted_users, whitelisted_users=whitelisted_users)
			else:
				return redirect(url_for('devices'))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))

@app.route('/devices/<int:dev_id>/bwl/changeBwl/<int:bwl_status>')
def change_bwl_status(dev_id,bwl_status):
	try:
		dev_id=int(dev_id)
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		else:
			ssid=session["ssid"]
			validation=server_app.devctrl.setBwlStatus(ssid,dev_id,bwl_status)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('show_bwl',dev_id=dev_id))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))
	
@app.route('/devices/<int:dev_id>/bwl/remove/blacklist/<int:usr_id>')
def bwl_remove_usr_from_blacklist(dev_id,usr_id):
	try:
		dev_id=int(dev_id)
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		else:
			ssid=session["ssid"]
			validation=server_app.devctrl.removeUserFromBlacklist(ssid,dev_id,usr_id)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('show_bwl',dev_id=dev_id))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))
	
@app.route('/devices/<int:dev_id>/bwl/remove/whitelist/<int:usr_id>')
def bwl_remove_usr_from_whitelist(dev_id,usr_id):
	try:
		dev_id=int(dev_id)
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		else:
			ssid=session["ssid"]
			validation=server_app.devctrl.removeUserFromWhitelist(ssid,dev_id,usr_id)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('show_bwl',dev_id=dev_id))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))
	
@app.route('/devices/<int:dev_id>/bwl/add/blacklist/<int:usr_id>')
def bwl_add_usr_to_blacklist(dev_id,usr_id):
	try:
		dev_id=int(dev_id)
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		else:
			ssid=session["ssid"]
			validation=server_app.devctrl.addUserToBlacklist(ssid,dev_id,usr_id)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('show_bwl',dev_id=dev_id))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))
	
@app.route('/devices/<int:dev_id>/bwl/add/whitelist/<int:usr_id>')
def bwl_add_usr_to_whitelist(dev_id,usr_id):
	try:
		dev_id=int(dev_id)
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('devices'))
		else:
			ssid=session["ssid"]
			validation=server_app.devctrl.addUserToWhitelist(ssid,dev_id,usr_id)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('show_bwl',dev_id=dev_id))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('devices'))
		
"""
USER
"""
@app.route('/users/')
def users():
	try:
		users=server_app.usrctrl.getAllItems()
		return render_template('users.html', server_app=server_app, users=users)
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('index'))

@app.route('/users/<int:usr_id>/')
def show_user(usr_id):
	try:
		user=server_app.usrctrl.getItemById(usr_id)
		if user:
			return render_template('show_user.html', server_app=server_app, session=session, user=user)
		else:
			flash("Es gibt diesen Nutzer nicht","warning")
			return redirect(url_for('users'))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('users'))

@app.route('/users/<int:usr_id>/upgrade/')
def upgrade(usr_id):
	try:
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('users'))
		else:
			ssid=session["ssid"]
			validation=server_app.usrctrl.upgrade(ssid,usr_id)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('show_user',usr_id=usr_id))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('show_user', usr_id=usr_id))

@app.route('/users/<int:usr_id>/downgrade/')
def downgrade(usr_id):
	try:
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('users'))
		else:
			ssid=session["ssid"]
			validation=server_app.usrctrl.downgrade(ssid,usr_id)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('show_user',usr_id=usr_id))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('show_user', usr_id=usr_id))

@app.route('/users/<int:usr_id>/reset_password/')
def reset_password(usr_id):
	try:
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('users'))
		else:
			ssid=session["ssid"]
			validation=server_app.usrctrl.resetPassword(ssid,usr_id)
			if not validation==True:
				flash(validation,'warning')
			return redirect(url_for('show_user',usr_id=usr_id))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('show_user', usr_id=usr_id))

@app.route('/users/<int:usr_id>/delete/')
def del_user(usr_id):
	try:
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('users'))
		else:
			ssid=session["ssid"]
			eingeloggter_user=server_app.usrctrl.getEingeloggterUser(ssid)
			validation=server_app.usrctrl.delItem(ssid,usr_id)
			if not validation==True:
				flash(validation,'warning')
				return redirect(url_for('show_user',usr_id=usr_id))
			else:
				if eingeloggter_user == usr_id:
					flash("You successfully deleted your account","success")
					flash("We will miss you","info")
					return redirect(url_for('logout'))
				else:
					flash("You successfully deleted the User","success")
					return redirect(url_for('users'))
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('show_user', usr_id=usr_id))
"""
ME
"""
@app.route('/me/')
def me():
	try:
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('index'))
		else:
			ssid=session["ssid"]
			user=server_app.usrctrl.getUserBySessionId(ssid)
			return render_template('me.html', server_app=server_app, session=session, user=user)
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('index'))

@app.route('/me/edit/', methods=['GET','POST'])
def edit_me():
	try:
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('index'))
		else:
			ssid=session["ssid"]
			user=server_app.usrctrl.getUserBySessionId(ssid)
			if request.method == 'POST':
				if "username" in request.form.keys():
					username=str(request.form['username'])
					if user.getUsername() != username:
						validation=server_app.usrctrl.setUsername(ssid,user.getId(),username)
						if validation:
							flash("You successfully updated your username","success")
						else:
							flash("The update of your username failed","warning")
				if "email" in request.form.keys():
					email=str(request.form['email'])
					if user.getEmail() != email:
						validation=server_app.usrctrl.setEmail(ssid,user.getId(),email)
						if validation:
							flash("You successfully updated your Email-Adress","success")
						else:
							flash("The update of your Email-Adress failed","warning")
				return redirect(url_for('me'))
			return render_template('edit_user.html', server_app=server_app, session=session, user=user)
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('index'))

@app.route('/me/change_password/', methods=['GET','POST'])
def change_my_password():
	try:
		if not check_login():
			flash('You must login to do that.', 'warning')
			return redirect(url_for('index'))
		else:
			ssid=session["ssid"]
			user=server_app.usrctrl.getUserBySessionId(ssid)
			if request.method == 'POST':
				if "old_password" in request.form.keys() and\
				   "new_password" in request.form.keys() and\
				   "password2" in request.form.keys():
					old_password=str(request.form['old_password'])
					password=str(request.form['new_password'])
					password2=str(request.form['password2'])
					if password == password2:
						validation=server_app.usrctrl.changePassword(user.getId(),old_password,password)
						if validation:
							flash("You successfully updated the password","success")
							return redirect(url_for('me'))
						else:
							flash("The update of the password failed","warning")
					else:
						flash('"Password" and "Type Password again" must be the same',"warning")
			return render_template('change_password.html', server_app=server_app, session=session, user=user)
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('index'))

"""
LOGIN
"""
@app.route('/login/', methods=['GET','POST'])
def login():
	try:
		if request.method == 'POST':
			if "username" in request.form.keys() and "password" in request.form.keys():
				username=request.form['username']
				password=request.form['password']
				ssid=server_app.usrctrl.login(username,password)
				if ssid:
					session['logged_in']=True
					session['ssid']=ssid
					flash("You're logged in!",'success')
					return redirect(url_for('index'))
				else:
					flash("Wrong Username or Password!",'warning')
			else:
				flash("Insert Username and Password!",'warning')
		return render_template('login.html', server_app=server_app, session=session)
	except Exception as e:
		flash(str(e), 'danger')
		return redirect(url_for('index'))

@app.route('/logout/')
def logout():
	try:
		if not "ssid" in session.keys():
			flash("It looks like you are not logged in, please contact an administator.","warning")
			return redirect(url_for('index'))
		ssid=session['ssid']
		server_app.usrctrl.logout(ssid)
		session.pop('ssid', None)
		session.pop('logged_in', None)
		flash("You've successfully logged out!","success")
		return redirect(url_for('index'))
	except Exception as e:
		flash(str(e), "danger")
		return redirect(url_for('index'))

@app.route('/register/', methods=['GET','POST'])
def register():
	try:
		if request.method == 'POST':
			if "username" in request.form.keys() and\
			   "password" in request.form.keys() and\
			   "email" in request.form.keys():
				username=request.form["username"]
				password=request.form["password"]
				email=request.form["email"]
				validation=False
				validation=server_app.usrctrl.createItem(username=username,password=password,email=email)
				if validation:
					flash("Your created a new account. Now login.","success")
					return redirect(url_for('login'))
			else:
				flash("You must post an username, a password and an email.","warning")
		return render_template('register.html', server_app=server_app, session=session)
	except Exception as e:
		flash(str(e), "danger")
		return render_template('register.html', server_app=server_app, session=session)
		
@app.route('/clients/add/', methods=['GET','POST'])
def add_client():
	try:
		if request.method == 'POST':
			if "username" in request.form.keys():
				username=request.form["username"]
			else:
				raise Exception("This action requires an username")
			if "password" in request.form.keys():
				password=request.form["password"]
			else:
				raise Exception("This action requires an password")
			ssid=server_app.usrctrl.login(username,password)
			if ssid:
				params={}
				params["ip_adress"]=request.remote_addr
				if "place" in request.form.keys():
					params["place"]=request.form["place"]
				validation=server_app.cctrl.createItem(ssid,**params)
				if validation:
					return str(server_app.cctrl.getKey(validation))
				else:
					return str(False)
			else:
				return "False: login failed"
		else:
			return str(False)
	except Exception as e:
		return "False: "+str(e)
