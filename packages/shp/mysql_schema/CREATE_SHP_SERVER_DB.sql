CREATE DATABASE IF NOT EXISTS `shp_server`;
USE `shp_server`;

CREATE TABLE IF NOT EXISTS `tbl_usr` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `username` VARCHAR(255) NOT NULL, #UNIQUE
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255), #UNIQUE
  `id_hierarchy_lvl` INT NOT NULL
);

CREATE TABLE IF NOT EXISTS `tbl_dev` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `sec_code` VARCHAR(5) NOT NULL, #UNIQUE
  `dev_code` INT NOT NULL, #UNIQUE
  `description` VARCHAR(255) NOT NULL,
  `place` VARCHAR(255),
  `status` BOOLEAN NOT NULL,
  `bwl_status` INT,
  `id_user` INT NOT NULL
);

CREATE TABLE IF NOT EXISTS `tbl_program` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `datetime` DATETIME NOT NULL,
  `description` VARCHAR(255),
  `status` BOOLEAN NOT NULL,
  `weekly` BOOLEAN NOT NULL,
  `inactive` BOOLEAN NOT NULL,
  `pin` INT(4),
  `id_user` INT NOT NULL,
  `id_device` INT NOT NULL
);

CREATE TABLE IF NOT EXISTS `tbl_grp` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `description` VARCHAR(255) NOT NULL, #UNIQUE
  `id_user` INT NOT NULL, #UNIQUE
  `private_grp` BOOLEAN
);

CREATE TABLE IF NOT EXISTS `tbl_grp_dev` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `id_device` INT NOT NULL,
  `id_group` INT NOT NULL
);

CREATE TABLE IF NOT EXISTS `tbl_bwl_list` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `id_device` INT NOT NULL,
  `bwl_status` INT NOT NULL
);

CREATE TABLE IF NOT EXISTS `tbl_bwl_list_usr` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `id_user` INT NOT NULL,
  `id_bwl_list` INT NOT NULL
);

CREATE TABLE IF NOT EXISTS `tbl_hierarchy_lvl` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `description` VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `tbl_permission` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `description` VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `tbl_role` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `id_permission` INT NOT NULL,
  `id_hierarchy_lvl` INT NOT NULL,
  `id_max_hierarchy_lvl` INT
);

CREATE TABLE IF NOT EXISTS `tbl_extra_role` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `id_permission` INT NOT NULL,
  `id_user` INT NOT NULL,
  `id_max_hierarchy_lvl` INT
);

CREATE TABLE IF NOT EXISTS `tbl_client` (
  `id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `place` VARCHAR(255),
  `ip_adress` VARCHAR(45) NOT NULL,
  `key` TEXT NOT NULL
);
