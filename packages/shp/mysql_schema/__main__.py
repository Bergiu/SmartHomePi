import __init__

question="""Options:

1. Create Client DB
2. Create Server DB
3. Insert Examples
"""
print(question)
x=int(input())
if x==1:
	username=input("MySql Username: ")
	password=input("MySql Password: ")
	print("Creating Client Database.")
	__init__.createShpClient(username,password)
elif x==2:
	username=input("MySql Username: ")
	password=input("MySql Password: ")
	print("Creating Server Database.")
	__init__.createShpServer(username,password)
elif x==3:
	username=input("MySql Username: ")
	password=input("MySql Password: ")
	print("Inserting some examples into Server Database.")
	__init__.insertExamples(username,password)
else:
	print("Operation not available!")

