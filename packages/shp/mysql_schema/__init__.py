import os

def createShpClient(db_username,db_password,host="localhost"):
	cwd=os.path.dirname(__file__)
	file="CREATE_SHP_CLIENT_DB.sql"
	print("MySql %s"%(file))
	os.popen("cat %s/%s | mysql -u %s -p%s --host=%s"%(cwd,file,db_username,db_password,host))
	
def createShpServer(db_username,db_password,host="localhost"):
	cwd=os.path.dirname(__file__)
	file1="CREATE_SHP_SERVER_DB.sql"
	file2="INSERT_REQUIRED.sql"
	print("MySql %s"%(file1))
	print("MySql %s"%(file2))
	os.popen("cat %s/%s %s/%s | mysql -u %s -p%s --host=%s"%(cwd,file1,cwd,file2,db_username,db_password,host))

def insertExamples(db_username,db_password,host="localhost"):
	cwd=os.path.dirname(__file__)
	file="INSERT_EXAMPLES.sql"
	print("MySql %s"%(file))
	os.popen("cat %s/%s | mysql -u %s -p%s --host=%s"%(cwd,file,db_username,db_password,host))

def createOwner(db_username,db_password,username,password,host="localhost"):
	query='INSERT INTO `tbl_usr` (`username`,`password`,`id_hierarchy_lvl`) value ("%s","%s",1)'%(username,password)
	os.popen('''echo '%s' | mysql -u %s -p%s --host=%s shp_server'''%(query,db_username,db_password,host))
	
def insertServer(db_username,db_password,ip_adress,key,host="localhost"):
	query='INSERT INTO `tbl_server` (`ip_adresss`,`key`) value ("%s","%s")'%(ip_adress,key)
	os.popen('''echo '%s' | mysql -u %s -p%s --host=%s shp_client'''%(query,db_username,db_password,host))
if __name__ == "__main__":
	pass
