USE `shp_server`;

INSERT INTO `tbl_usr` (`username`,`password`,`email`,`id_hierarchy_lvl`)
VALUES
("Bergiu", "Passwort1", "marcoschlicht@yahoo.de",1),
("Phillip", "Passwort2", "phillip@yahoo.de",3),
("Spid3r", "Passwd", "pb.games@outlook.de",2);

INSERT INTO `tbl_dev` (`sec_code`,`dev_code`,`description`, `status`, `id_user`)
VALUES
(16, 1, "Pierre Steckdose A", 0, 1),
(16, 2, "Pierre Steckdose B", 0, 1),
(16, 3, "Kaffeemaschine", 0, 1),
(16, 4, "Wohnzimmerlampe", 0, 1),
(16, 5, "Tannenbaum", 0, 3),
(16, 6, "Elektronische Silversterraketen", 0, 3);

INSERT INTO `tbl_grp` (`description`)
VALUES
("Wohnzimmer"),
("Spielzimmer"),
("Lampen");

INSERT INTO `tbl_grp_dev` (`id_device`,`id_group`)
VALUES
(1,2),
(1,3),
(2,2),
(3,1),
(3,3),
(4,1);

INSERT INTO `tbl_program` (`datetime`,`status`,`weekly`,`inactive`,`id_user`,`id_device`)
VALUES
('2015-12-12 15:30:00',1,0,0,1,4),
('2016-01-01 00:00:00',1,0,0,1,5);

