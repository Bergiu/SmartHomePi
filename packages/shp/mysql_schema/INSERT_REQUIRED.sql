USE `shp_server`;

INSERT INTO `tbl_hierarchy_lvl` (`id`,`description`)
VALUES
(1,"Owner"),
(2,"Admin"),
(3,"Member"),
(4,"Guest");

INSERT INTO `tbl_permission` (`id`,`description`)
VALUES
(1,"user.resetPwd"),
(2,"user.deleteUsr"),
(3,"globalGroup.createGrp"),
(4,"globalGroup.updateGrp"),
(5,"globalGroup.deleteGrp"),
(6,"privateGroup.createGrp"),
(7,"privateGroup.updateGrp"),
(8,"privateGroup.deleteGrp"),
(9,"privateGroup.seeGrp"),
(10,"device.createDev"),
(11,"device.changeDevStatus"),
(12,"device.changeDevData"),
(13,"device.deleteDev"),
(14,"device.addUsrToBwl"),
(15,"program.changeProgram"),
(16,"program.deleteProgram"),
(17,"program.seePin"),
(18,"hierarchyLvl.upgrade"),
(19,"hierarchyLvl.downgrade"),
(20,"extraPermission.dealWithPermissions"),
(21,"client.createClient"),
(22,"client.changeClientPlace"),
(23,"client.resetKey"),
(24,"client.deleteClient");

#Permissions with relation to other hierarchy_lvl
INSERT INTO `tbl_role` (`id`,`id_permission`,`id_hierarchy_lvl`,`id_max_hierarchy_lvl`)
VALUES
(1,1,1,2),
(2,1,2,2),
(3,2,1,2),
(4,2,2,3),
(5,4,1,2),
(6,4,2,2),
(7,5,1,2),
(8,5,2,2),
(9,7,1,2),
(10,7,2,3),
(11,8,1,2),
(12,8,2,3),
(13,9,1,2),
(14,9,2,2),
(15,11,1,1),
(16,11,2,1),
(17,11,3,1),
(18,12,1,1),
(19,12,2,1),
(20,13,1,2),
(21,13,2,2),
(22,14,1,2),
(23,14,2,3),
(24,15,1,1),
(25,15,2,1),
(26,15,3,1),
(27,16,1,1),
(28,16,2,1),
(29,16,3,1),
(30,17,1,2),
(31,17,2,3),
(32,18,1,3),
(33,18,2,4),
(34,19,1,2),
(35,19,2,3),
(36,20,1,2);

#Permissions without relation to other hierarchy_lvl
INSERT INTO `tbl_role` (`id`,`id_permission`,`id_hierarchy_lvl`)
VALUES
(37,3,1),
(38,3,2),
(39,6,1),
(40,6,2),
(41,6,3),
(42,6,4),
(43,10,1),
(44,10,2),
(45,21,1),
(46,22,1),
(47,23,1),
(48,24,1);
