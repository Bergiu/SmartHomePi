#
import json
import os
import atexit
class Process( object):
	"""
	
	"""
	#filename=os.path.dirname(__file__)+"/.tmp/tmp_updates.txt"
	filename="/tmp/shp_updates"
	
	new_process={
	  "tbl_permission":[],
	  "tbl_role":[],
	  "tbl_extra_role":[],
	  "tbl_hierarchy_lvl":[],
	  "tbl_usr":[],
	  "tbl_bwl_list_usr":[],
	  "tbl_bwl_list":[],
	  "tbl_program":[],
	  "tbl_grp":[],
	  "tbl_grp_dev":[],
	  "tbl_dev":[],
	  "tbl_client":[],
	  "tbl_server":[]
	}
	def __init__(self):
		print("__Start new Process__")
		isfile=os.path.isfile(self.filename)
		id=-1
		if isfile:
			file=self.readFile()
			for process in file:
				if process["id"] > id:
					id=process["id"]
		else:
			#createFile
			file=[]
		id+=1
		new_process=self.new_process
		new_process["id"]=id
		file.append(self.new_process)
		self.writeFile(file)
		self.id_process=new_process["id"]
		"""
		 führt __endProcess bei desctruction des Prozesses aus
		"""
		atexit.register(self.__endProcess)
	
	def __endProcess(self):
		"""
		 Called on destruction of the Process and removes the Process from the update File
		"""
		print("__End Process__")
		file=self.readFile()
		idx=self.getProcessIdx(file)
		del(file[idx])
		self.writeFile(file)
	
	def getProcessIdx(self, file):
		idx=0
		for process in file:
			if process["id"]==self.id_process:
				return idx
			else:
				idx+=1
		return 0
	
	def readFile(self):
		file_in=os.popen("cat %s"%(self.filename)).read()
		file=json.loads(file_in)
		return file
		
	def writeFile(self, file):
		file_out=json.dumps(file)
		os.popen('''echo '%s' > %s'''%(file_out,self.filename))
		return True
	
	def isUpToDate(self,update_type,id_object):
		file=self.readFile()
		idx=self.getProcessIdx(file)
		if update_type in file[idx].keys():
			if id_object in file[idx][update_type]:
				return False
			else:
				return True
		else:
			raise Exception("Process: There is no %s array in %s"%(update_type,self.filename))
	
	def setUpToDate(self,update_type,id_object):
		file=self.readFile()
		idx=self.getProcessIdx(file)
		if update_type in file[idx].keys():
			if id_object in file[idx][update_type]:
				#print("OLD FILE: ____\n%s\n____"%(file))
				#print("file[%s][%s].remove(%s)"%(self.id_process,update_type,id_object))
				#print(file[self.id_process][update_type])
				file[idx][update_type].remove(id_object)
				#print("NEW FILE: ____\n%s\n____"%(file))
			else:
				return False
		else:
			raise Exception("Process: There is no %s array in %s"%(update_type,self.filename))
		self.writeFile(file)
	
	def setUpdated(self,update_type,id_object):
		file=self.readFile()
		idx=self.getProcessIdx(file)
		for idx_process in range(len(file)):
			if not idx_process==idx:
				#print("saving in %s"%(idx_process))
				if update_type in file[idx_process].keys():
					if -id_object in file[idx_process][update_type]:
						#print("es war negativ in dem array")
						file[idx_process][update_type].remove(-id_object)
						file[idx_process][update_type].append(id_object)
					elif not id_object in file[idx_process][update_type]:
						file[idx_process][update_type].append(id_object)
					else:
						#print("Es ist schon in %s"%(idx_process))
						pass
				else:
					raise Exception("Process: There is no %s array in %s"%(update_type,self.filename))
			else:
				#print("übersprungen")
				pass
		self.writeFile(file)
				
	def getUpdates(self,update_type):
		file=self.readFile()
		idx=self.getProcessIdx(file)
		updates=[]
		if update_type in file[idx].keys():
			for id_object in file[idx][update_type]:
				updates.append(id_object)	
		else:
			raise Exception("Process: There is no %s array in %s"%(update_type,self.filename))
		return updates
