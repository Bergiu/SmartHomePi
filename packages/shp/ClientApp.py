import os
from .client.ServerController import ServerController
from .Process import Process
from .MySqlDatabase import MySqlDatabase

class ClientApp(object):
	def __init__(self, **kwargs):
		params={}
		if "DB_USERNAME" in kwargs.keys():
			params["DB_USERNAME"]=kwargs["DB_USERNAME"]
		else:
			raise KeyError("ClientApp: __init__: missing keyword DB_USERNAME")
		if "DB_PASSWORD" in kwargs.keys():
			params["DB_PASSWORD"]=kwargs["DB_PASSWORD"]
		else:
			raise KeyError("ClientApp: __init__: missing keyword DB_PASSWORD")
		if "SEND_CMD" in kwargs.keys():
			self.SEND_CMD=kwargs["SEND_CMD"]
		else:
			self.SEND_CMD=""
		print("Initiating new client application")
		proc=Process()
		db=MySqlDatabase(proc,"shp_client",params["DB_USERNAME"],params["DB_PASSWORD"])
		servctrl=ServerController(proc,db)
		
		self.proc=proc
		self.db=db
		self.servctrl=servctrl
	
	def send(self, **kwargs):
		"""
		Turns a device on or off
		@**kwargs:
		 ip_server:string
		 key:string
		 dev_code:int
		 sec_code:int
		 status:boolean
		"""
		missing="ClientApp send: Missing "
		if not "ip_server" in kwargs.keys():
			raise ValueError(missing+"ip_server")
		if not "key" in kwargs.keys():
			raise ValueError(missing+"key")
		if not "dev_code" in kwargs.keys():
			raise ValueError(missing+"dev_code")
		if not "sec_code" in kwargs.keys():
			raise ValueError(missing+"sec_code")
		if not "status" in kwargs.keys():
			raise ValueError(missing+"status")
		ip_server=kwargs["ip_server"]
		key=kwargs["key"]
		dev_code=int(kwargs["dev_code"])
		sec_code=str(kwargs["sec_code"])
		status=int(kwargs["status"])
		
		id_server=self.servctrl.getIdByIp(ip_server)
		if id_server:
			validation=self.servctrl.checkKey(id_server, key)
		
			if validation:
				if self.SEND_CMD:
					send=self.SEND_CMD
					send_cmd="sudo %s %s %s %s"%(send, kwargs["sec_code"], kwargs["dev_code"], int(kwargs["status"]))
					print(send_cmd)
					#TODO
					os.popen(send_cmd)
					return True
				else:
					return "Error: SEND_CMD not set"
			else:
				return "Error: Wrong key"
		else:
			return "Error: No server with ip: %s"%(ip_server)	
				
				
				
				
