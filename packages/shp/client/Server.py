# 
class Server (  ):
	#private:
	"""
	id # int
	ip_adress # string
	key # string
	"""

	#public:
	def __init__( self, **kwargs):
		"""
		 @**kwargs:
		  id:int
		  ip_adress:string
		  key:string
		"""
		missing="Server __init__: Missing "
		if "id" in kwargs.keys():
			self.id=int(kwargs["id"])
		else:
			raise ValueError(missing+"id")
		if "ip_adress" in kwargs.keys():
			self.ip_adress=str(kwargs["ip_adress"])
		else:
			raise ValueError(missing+"ip_adress")
		if "key" in kwargs.keys():
			self.key=str(kwargs["key"])
		else:
			raise ValueError(missing+"key")
	
	def getId( self): 
		"""
		 @id:int
		"""
		return self.id

	def getIpAdress( self): 
		"""
		 @ip_adress:string
		"""
		return self.ip_adress

	def setIpAdress( self, ip_adress):
		"""
		 @ip_adress:string
		 @validation:boolean
		"""
		self.ip_adress=str(ip_adress)

	def checkKey( self, key): 
		"""
		 @key:text
		 @validation:boolean
		"""
		if self.key==key:
			return True
		else:
			return False

	def setKey( self, key):
		"""
		 @key:string
		 @validation:boolean
		"""
		self.key=str(key)
		return True

