# 
from ..Controller import Controller
from .Server import Server
class ServerController ( Controller ):

	table_name="tbl_server"

	def __init__( self, process, database):
		"""
		 @database: Database
		 @**kwargs
		  user_controller: UserController
		  program_controller: ProgramController
		  bwl_list_controller: BwlListController
		"""
		super(ServerController,self).__init__(process,database)
		self.initiateAllFromDB()

	def createItem(self, **kwargs):
		"""
		 @**kwargs:
		  ip_adress:string
		  key:string
		"""
		missing="ServerController createItem: Missing "
		params={}
		if "ip_adress" in kwargs.keys():
			params["ip_adress"]=str(kwargs["ip_adress"])
		else:
			raise ValueError(missing+"ip_adress")
		if "key" in kwargs.keys():
			params["key"]=str(kwargs["key"])
		else:
			raise ValueError(missing+"key")
			
		duplicate=self.getIdByIp(params["ip_adress"])
		if duplicate:
			raise Exception("ServerController createItem: The server %s is already in use")
		params["id"]=int(self.database.insert(self.table_name,**params))
		
		item=Server(**params)
		validation=self.addItem(item)
		if validation:
			return item.getId()
		else:
			return False

	def delItem( self, id_item ): 
		"""
		 @id_item:int
		"""
		item=self.getItemById(id_item)
		if item:
			validation=self.database.delete(self.table_name,id_item)
			if not validation:
				return False
			self.itemList.remove(item)
			self.indexIdx-=1
			return True
		else:
			raise Exception("No Server with id=%s"%(id_item))
	
	def initiateFromDB( self, id_item ): 
		"""
		 @id_item:int
		 @validation:boolean
		"""
		item_data=self.database.load(self.table_name,id_item)
		if item_data:
			params={}
			params["id"]=int(item_data[0])
			duplicate=self.isItem(params["id"])
			if duplicate:
				self.removeItem(params["id"])
			params["ip_adress"]=str(item_data[1])
			params["key"]=str(item_data[2])
			item=Server(**params)
			self.addItem(item)
			return True
		else:
			return False

	def initiateAllFromDB( self ): 
		"""
		 @validation:boolean
		"""
		item_datas=self.database.loadTable(self.table_name)
		for item_data in item_datas:
				params={}
				params["id"]=int(item_data[0])
				duplicate=self.isItem(params["id"])
				if duplicate:
					self.removeItem(params["id"])
				params["ip_adress"]=str(item_data[1])
				params["key"]=str(item_data[2])
				item=Server(**params)
				self.addItem(item)
		return True
	#end interface

	#public:
	def getId( self, idx_server): 
		"""
		 @idx_server:int
		 @id:int
		"""
		if len(self.itemList)>idx_server:
			return self.itemList[idx_server].getId()
		else:
			return False

	def getIdByIp(self, ip_adress):
		for item in self.itemList:
			if item.getIpAdress() == str(ip_adress):
				return item.getId()
		return False

	def getIpAdress( self, id_server): 
		"""
		 @id_server:int
		 @ip_adress:string
		"""
		item=self.getItemById(id_server)
		if item:
			return item.getIpAdress()
		else:
			return False

	def checkKey( self, id_server,  key): 
		"""
		 @id_server:int
		 @key:text
		 @validation:boolean
		"""
		item=self.getItemById(id_server)
		if item:
			return item.checkKey(key)
		else:
			return False

