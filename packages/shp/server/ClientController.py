# 
import os
import urllib.request, urllib.parse, urllib.error
import socket
from ..Controller import Controller
from .Client import Client
from .UserController import UserController
class ClientController ( Controller ):
	
	"""
	user_controller # UserController
	"""
	
	#private:
	table_name="tbl_client"	
	process_name="Client"

	def __init__( self, process, database, **kwargs):
		super(ClientController,self).__init__(process,database)
		if "user_controller" in kwargs.keys():
			self.user_controller=kwargs["user_controller"]
		else:
			self.user_controller=""
		self.initiateAllFromDB()

	#interface
	def createItem(self, session_id, **kwargs):
		"""
		 @**kwargs:
		  ip_adress: string
		  place: string
		"""
		perm="client.createClient"
		perm_id=21
		missing="ClientController createItem: Missing "
		params={}
		
		if not self.user_controller:
			raise Exception("You must set the user_controller to create new Clients.")
		if not self.user_controller.selfHasPermission(session_id, perm_id):
			raise Exception("No Permission to create Client.")
		
		if "ip_adress" in kwargs.keys():
			params["ip_adress"]=kwargs["ip_adress"]
		else:
			raise ValueError(missing+"ip_adress")
		if "place" in kwargs.keys():
			params["place"]=kwargs["place"]
		else:
			params["place"]=""
		params["key"]=ClientController.generateKey()
		
		duplicate=self.getClientByIpAdress(params["ip_adress"])
		if not duplicate:
			params["id"]=self.database.insert(self.table_name,**params)
			item=Client(**params)
			validation=self.addItem(item)
			if validation:
				return item.getId()
			else:
				return False
		else:
			raise Exception("Ip Adress is already in the System.")

	def delItem( self, session_id, id_item ): 
		"""
		 @id_item:int
		"""
		perm="client.deleteClient"
		perm_id=24
		item=self.getItemById(id_item)
		id_eingeloggter_user=self.user_controller.getEingeloggterUser(session_id)
		if not item:
			raise Exception("No Client with id=%s"%(id_item))
		if not eingeloggter_user:
			raise Exception("You must be logged in to Delete Clients")
		if not self.user_controller:
			raise Exception("You must set the user_controller to Delete Clients.")
		if self.user_controller.selfHasPermission(session_id, perm_id) :
			validation=self.database.delete(self.table_name,id_item)
			if not validation:
				return False
			self.itemList.remove(item)
			self.indexIdx-=1
			return True
		else:
			raise Exception("No Permission to delete Clients")
	
	def initiateFromDB( self, id_item ): 
		"""
		 @id_item:int
		"""
		item_data=self.database.load(self.table_name,id_item)
		if item_data:
			params={}
			params["id"]=int(item_data[0])
			duplicate=self.isItem(id)
			if duplicate:
				self.removeItem(id)
			params["place"]=str(item_data[1])
			params["ip_adress"]=str(item_data[2])
			params["key"]=str(item_data[3])
			item=Client(**params)
			self.addItem(item)
			return True
			
	def initiateAllFromDB( self ): 
		"""
		"""
		item_datas=self.database.loadTable(self.table_name)
		for item_data in item_datas:
			params={}
			params["id"]=int(item_data[0])
			duplicate=self.isItem(id)
			if duplicate:
				self.removeItem(id)
			params["place"]=str(item_data[1])
			params["ip_adress"]=str(item_data[2])
			params["key"]=str(item_data[3])
			item=Client(**params)
			self.addItem(item)
		return True
	#end interface

	def getClientByIpAdress(self, ip_adress):
		"""
		 @ip_adress:string
		"""
		client=False
		for item in self.itemList:
			if item.getIpAdress() == ip_adress:
				client=item
		if client:
			return client
		else:
			id_client=self.database.loadIdByAttr(self.table_name,"ip_adress",ip_adress)
			if id_client:
				client=self.getItemById(id_client)
				return client
			else:
				return False
	
	def generateKey():
		key=os.popen("openssl dhparam 512 2> /dev/null").read()
		return key
		
	#public:
	def getIpAdress( self, id_client): 
		"""
		 @id_client:int
		 @ip_adress:string
		"""
		item=self.getItemById(id_client)
		if item:
			return item.getIpAdress()
		else:
			return None

	def getPlace( self, id_client): 
		"""
		 @id_client:int
		 @place:string
		"""
		item=self.getItemById(id_client)
		if item:
			return item.getPlace()
		else:
			return None

	def getKey( self, id_client): 
		"""
		 @id_client:int
		 @key:text
		"""
		item=self.getItemById(id_client)
		if item:
			return item.getKey()
		else:
			return None

	def sendStatusToAll( self, sec_code, dev_code, status):
		for client in self.getAllItems():
			id_client=client.getId()
			self.sendStatus(id_client, sec_code, dev_code, status)
		#man könnte hier noch was prüfen
		return True

	def sendStatus( self, id_client, sec_code, dev_code, status):
		client=self.getItemById(id_client)
		if not client:
			raise Exception("There is no client with id=%s"%(id_client))
		ip_client=str(client.getIpAdress())
		params={}
		params["key"]=str(client.getKey())
		params["sec_code"]=str(sec_code)
		params["dev_code"]=int(dev_code)
		params["status"]=int(status)
		try:
			details = urllib.parse.urlencode(params)
			binary_details=details.encode('utf8')
			url = urllib.request.Request('http://'+ip_client+':5523/send', binary_details)
			url.add_header("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.29 Safari/525.13")
			responseData = urllib.request.urlopen(url).read().decode('utf8', 'ignore')
			responseFail = False
			return responseData

		except urllib.error.HTTPError as e:
		    responseData = e.read().decode('utf8', 'ignore')
		    responseFail = False

		except urllib.error.URLError:
		    responseFail = True

		except socket.error:
		    responseFail = True

		except socket.timeout:
		    responseFail = True

		except UnicodeEncodeError:
		    print("[x]  Encoding Error")
		    responseFail = True

	
	def listClients(self):
		for device in self.getAllItems():
			print("%s (%s)"%(device.getIpAdress(),device.getPlace()))
		
