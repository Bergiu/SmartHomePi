# 
from ..Controller import Controller
from .BwlList import BwlList
from .BwlListUserController import BwlListUserController
class BwlListController ( Controller ):
	#private:
	table_name="tbl_bwl_list"
	
	def __init__( self, process, database, **kwargs):
		"""
		 Initiates a new BlackWhiteList Controller
		 
		 @database: Database
		 @**kwargs:
		  user_controller: UserController
		  bwl_list_user_controller: BwlListUserController
		"""
		super(BwlListController,self).__init__(process, database)
		if "user_controller" in kwargs.keys():
			self.user_controller=kwargs["user_controller"]
			self.bwl_list_user_controller=BwlListUserController(process,database,user_controller=kwargs["user_controller"])
		else:
			self.user_controller=""
			self.bwl_list_user_controller=BwlListUserController(process,database)
		#self.initiateAllFromDB()

	def delItem( self, id_item ): 
		"""
		 Deletes a black- or whitelist
		 
		 @id_item:int
		"""
		item=self.getItemById(id_item)
		if item:
			validation=self.database.delete(self.table_name,id_item)
			if not validation:
				return False
			self.itemList.remove(item)
			self.indexIdx-=1
			return True
		else:
			raise Exception("No BwlList with id=%s"%(item.getId()))
	
	def createItem(self,**kwargs):
		"""
		 Creates a new black- or whitelist for a device
		 
		 @**kwargs
		  id_device:int
		  bwl_status:int (1..3)
		    1:=Blacklist
		    2:=Whitelist
		    3:=Undefinedlist
		"""
		missing="BwlListController createBwlList: Missing "
		params={}
		if "id_device" in kwargs.keys():
			params["id_device"]=int(kwargs["id_device"])
		else:
			raise ValueError(missing+"id_device")
		if "bwl_status" in kwargs.keys():
			params["bwl_status"]=int(kwargs["bwl_status"])
		else:
			raise ValueError(missing+"bwl_status")
		params["id"]=self.database.insert(self.table_name,**params)
		item=BwlList(**params)
		validation=self.addItem(item)
		if validation:
			return item.getId()
		else:
			return False

	def initiateFromDB( self, id_item ): 
		"""
		 Initiates one black- or whitelist from the database
		 
		 @id_item:int
		"""
		if self.bwl_list_user_controller:
			item_data=self.database.load(self.table_name,id_item)
			if item_data:
				params={}
				params["id"]=int(item_data[0])
				duplicate=self.isItem(id)
				if duplicate:
					self.removeItem(id)
				params["id_device"]=str(item_data[1])
				params["bwl_status"]=str(item_data[2])
				table_name_bwl_list_user=self.bwl_list_user_controller.getTableName()
				bwl_list_user_ids=self.database.loadForeignKeys(table_name_bwl_list_user,"id_bwl_list",self.table_name,id_item)
				params["ids_bwl_list_user"]=[]
				for bwl_list_user_id in bwl_list_user_ids:
					if self.bwl_list_user_controller.getItemById(int(bwl_list_user_id)):
						params["ids_bwl_list_user"].append(int(bwl_list_user_id))
					else:
						print("no bwl_list_user with id %s"%(bwl_list_user_id))
				item=BwlList(**params)
				self.addItem(item)
				return True
			else:
				return False
		else:
			return False

	def initiateAllFromDB( self ): 
		"""
		 Initiates all black- and whitelist from the database
		"""
		if self.bwl_list_user_controller:
			item_datas=self.database.loadTable(self.table_name)
			for item_data in item_datas:
				params={}
				params["id"]=int(item_data[0])
				duplicate=self.isItem(id)
				if duplicate:
					self.removeItem(id)
				params["id_device"]=int(item_data[1])
				params["bwl_status"]=int(item_data[2])
				table_name_bwl_list_user=self.bwl_list_user_controller.getTableName()
				bwl_list_user_ids=self.database.loadForeignKeys(table_name_bwl_list_user,"id_bwl_list",self.table_name,params["id"])
				params["ids_bwl_list_user"]=[]
				for bwl_list_user_id in bwl_list_user_ids:
					if self.bwl_list_user_controller.getItemById(int(bwl_list_user_id)):
						params["ids_bwl_list_user"].append(int(bwl_list_user_id))
					else:
						print("no bwl_list_user with id %s"%(bwl_list_user_id))
				item=BwlList(**params)
				self.addItem(item)
			return True
		else:
			return False
	#end interface

	#public:
	def getDevice( self, id_bwl_list): 
		"""
		 @id_bwl_list:int
		 @ids_bwl_list_user:array
		"""
		item=self.getItemById(id_bwl_list)
		return item.getDevice()
		
	def getBwlStatus( self, id_bwl_list): 
		"""
		 @id_bwl_list:int
		 @ids_bwl_list_user:array
		"""
		item=self.getItemById(id_bwl_list)
		return item.getBwlStatus()
		
	def getBwlListUsers( self, id_bwl_list): 
		"""
		 Returns all ids of the BwlListUsers that are attached to an BwlList with the id = id_bwl_list
		 
		 @id_bwl_list:int
		 @ids_bwl_list_user:array
		"""
		item=self.getItemById(id_bwl_list)
		return item.getBwlListUsers()

	def addBwlListUser( self, id_bwl_list, id_bwl_list_user):
		"""
		
		"""
		if not self.bwl_list_user_controller:
			raise Exception("You must set the bwl_list_user_controller.")
		bwl_list=self.getItemById(id_bwl_list)
		if not bwl_list:
			raise Exception("There is no BwlList with ID=%s"%(id_bwl_list))
		if not self.bwl_list_user_controller.getItemById(id_bwl_list_user):
			raise Exception("There is no BwlListUser with ID=%s"%(id_bwl_list_user))
		validation=bwl_list.addBwlListUser(id_bwl_list_user)
		return validation
		
	def removeBwlListUser( self, id_bwl_list, id_bwl_list_user):
		"""
		
		"""
		bwl_list=self.getItemById(id_bwl_list)
		if not bwl_list:
			raise Exception("There is no BwlList with ID=%s"%(id_bwl_list))
		validation=bwl_list.removeBwlListUser(id_bwl_list_user)
		return validation

	def addUserToList( self, id_bwl_list,  id_user):
		"""
		 Alias createBwlListUser()
		 @id_bwl_list:int
		 @id_bwl_list_user:int
		"""
		perm="device.manageBwl"
		perm_id=14
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		if not self.bwl_list_user_controller:
			raise Exception("You must set the bwl_list_user_controller.")
		bwl_list=self.getItemById(id_bwl_list)
		if not bwl_list:
			raise Exception("There is no BwlList with ID=%s"%(id_bwl_list))
		id_bwl_list_user=self.bwl_list_user_controller.createItem(id_user=id_user,id_bwl_list=id_bwl_list)
		if id_bwl_list_user:
			validation=self.addBwlListUser(id_bwl_list, id_bwl_list_user)
			return validation
		else:
			return False

	def removeUserFromList( self, id_bwl_list, id_user): 
		"""
		 Alias delBwlListUser()
		 @id_bwl_list:int
		 @id_bwl_list_user:int
		"""
		perm="device.manageBwl"
		perm_id=14
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		if not self.bwl_list_user_controller:
			raise Exception("You must set the bwl_list_user_controller.")
		bwl_list=self.getItemById(id_bwl_list)
		if not bwl_list:
			raise Exception("There is no BwlList with ID=%s"%(id_bwl_list))
		id_bwl_list_user=self.bwl_list_user_controller.getIdByIdUserAndIdBwlList(id_user,id_bwl_list)
		if id_bwl_list_user:
			validation=self.bwl_list_user_controller.delItem(id_bwl_list_user)
			if validation:
				validation=self.removeBwlListUser(id_bwl_list, id_bwl_list_user)
				return validation
		else:
			return False

	def getListedUsers( self, id_bwl_list ):
		"""
		 Returns all user_ids from a bwl_list
		 @id_bwl_list:int
		"""
		if not self.bwl_list_user_controller:
			raise Exception("You must set the bwl_list_user_controller.")
		if not self.getItemById(id_bwl_list):
			raise Exception("There is no BwlList with ID=%s"%(id_bwl_list))
			
		ids_bwl_list_user=self.getBwlListUsers(id_bwl_list)
		ids_listed_user=[]
		if not ids_bwl_list_user:
			print("There are no Users listed")
			return False
		for id_bwl_list_user in ids_bwl_list_user:
			ids_listed_user.append(self.bwl_list_user_controller.getUser(id_bwl_list_user))
		if ids_listed_user:
			return ids_listed_user
		else:
			return False

	def isUserListed( self, id_bwl_list, id_user):
		ids_listed_user=self.getListedUsers(id_bwl_list)
		if not ids_listed_user:
			return False
		elif id_user in ids_listed_user:
			return True
		else:
			return False
	
	def setBwlListUserController( self, bwl_list_user_controller): 
		"""
		 @user_controller:BwlListUserController
		"""
		if self.user_controller==user_controller:
			validation=self.bwl_list_user_controller.setUserController(user_controller)
			return validation
		else:
			return False


