# 
class BwlListUser (  ):
	#private:
	"""
	id # int
	id_device # int
	id_user # int
	"""
	#public:
	def __init__(self,**kwargs):
		"""
		 @**kwargs:
		  id: int
		  id_bwl_list: int
		  id_user: int
		"""
		missing="BwlListUser __init__: Missing "
		
		if "id" in kwargs.keys():
			self.id=int(kwargs["id"])
		else:
			raise ValueError(missing+"id")
		if "id_bwl_list" in kwargs.keys():
			self.id_bwl_list=int(kwargs["id_bwl_list"])
		else:
			raise ValueError(missing+"id_bwl_list")
		if "id_user" in kwargs.keys():
			self.id_user=int(kwargs["id_user"])
		else:
			raise ValueError(missing+"id_user")
	
	def getId( self):
		"""
		 @id:int
		"""
		return self.id
	
	def getBwlList( self):
		"""
		 @id_bwl_list: int
		"""
		return self.id_bwl_list
	
	def getUser( self):
		"""
		 @id_user:int
		"""
		return self.id_user
