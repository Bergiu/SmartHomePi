# 
class BwlList (  ):
	#private:
	"""
	id_related_dev # int
	ids_bwl_list_user # array
	"""
	#public:
	def __init__(self,**kwargs):
		"""
		 @**kwargs:
		  id: int
		  id_device: int
		  bwl_status: int
		  ids_bwl_list_user: array = []
		"""
		missing="BwlList __init__: Missing "
		
		if "id" in kwargs.keys():
			self.id=int(kwargs["id"])
		else:
			raise ValueError(missing+"id")
		if "id_device" in kwargs.keys():
			self.id_device=int(kwargs["id_device"])
		else:
			raise ValueError(missing+"id_device")
		if "bwl_status" in kwargs.keys():
			if int(kwargs["bwl_status"]) <= 0b11:
				self.bwl_status=int(kwargs["bwl_status"])
			else:
				raise ValueError("BwlList __init__: bwl_status is not valid")
		else:
			raise ValueError(missing+"bwl_status")
		if "ids_bwl_list_user" in kwargs.keys():
			self.ids_bwl_list_user=list(kwargs["ids_bwl_list_user"])
		else:
			self.ids_bwl_list_user=[]
	
	def getId( self):
		"""
		 @id:int
		"""
		return self.id
	
	def getDevice( self):
		"""
		 @id_device: int
		"""
		return self.id_device
	
	def getBwlStatus( self):
		"""
		 @bwl_status: int
		"""
		return self.bwl_status
	
	def getBwlListUsers( self): 
		"""
		 @id_bwl_list_users:array
		"""
		return self.ids_bwl_list_user

	def addBwlListUser( self, id_bwl_list_user): 
		"""
		 @id_bwl_list_user:int
		"""
		if int(id_bwl_list_user) in self.ids_bwl_list_user:
			return False
		else:
			self.ids_bwl_list_user.append(int(id_bwl_list_user))
			return True

	def removeBwlListUser( self, id_bwl_list_user): 
		"""
		 @id_bwl_list_user:int
		"""
		if int(id_bwl_list_user) in self.ids_bwl_list_user:
			self.ids_bwl_list_user.remove(int(id_bwl_list_user))
			return True
		else:
			return False


