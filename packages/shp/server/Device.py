# 
class Device (  ):
	#private:
	"""
	id # int
	description # string
	sec_code # bit(5)
	dev_code # int
	place # string
	status # boolean
	bwl_status # bit(2)
	id_blacklist # int
	id_whitelist # int
	id_undefinedbwllist # int
	"""

	#public:
	def __init__(self, **kwargs):
		"""
		 @**kwargs:
		  id:int
		  description:string
		  sec_code:int
		  dev_code:int
		  place:string = ""
		  status:boolean = 0
		  id_user
		  bwl_status:bit(2) = 0b00
		  id_blacklist:int = ""
		  id_whitelist:int = ""
		  id_undefinedbwllist:int = ""
		"""
		err_func="Device __init__: "
		missing="Device __init__: Missing "
		if "id" in kwargs.keys():
			self.id=int(kwargs["id"])
		else:
			raise ValueError(missing+"id")
		if "description" in kwargs.keys():
			self.description=str(kwargs["description"])
		else:
			raise ValueError(missing+"description")
		if "sec_code" in kwargs.keys():
			"""
			if int(kwargs["sec_code"]) <= 0b11111:
				self.sec_code=int(kwargs["sec_code"])
			else:
				raise ValueError(err_func+"sec_code must be between 0 and 31")
			"""
			self.sec_code=str(kwargs["sec_code"])
		else:
			raise ValueError(missing+"sec_code")
		if "dev_code" in kwargs.keys():
			self.dev_code=int(kwargs["dev_code"])
		else:
			raise ValueError(missing+"dev_code")
		if "place" in kwargs.keys():
			self.place=str(kwargs["place"])
		else:
			self.place=""
		if "status" in kwargs.keys():
			self.status=bool(kwargs["status"])
		else:
			self.place=0
		if "id_user" in kwargs.keys():
			self.id_user=int(kwargs["id_user"])
		else:
			raise ValueError(missing+"id_user")
		if "bwl_status" in kwargs.keys():
			if int(kwargs["bwl_status"]) <= 0b11:
				self.bwl_status=int(kwargs["bwl_status"])
			else:
				raise ValueError(err_func+"bwl_status must be between 0 and 3")
		else:
			self.bwl_status=bin(0b00)
		if "id_blacklist" in kwargs.keys():
			self.id_blacklist=int(kwargs["id_blacklist"])
		else:
			self.id_blacklist=""
		if "id_whitelist" in kwargs.keys():
			self.id_whitelist=int(kwargs["id_whitelist"])
		else:
			self.id_whitelist=""
		if "id_undefinedbwllist" in kwargs.keys():
			self.id_undefinedbwllist=int(kwargs["id_undefinedbwllist"])
		else:
			self.id_undefindebwllist=""
	
	def getId( self): 
		"""
		 @id:int
		"""
		return self.id

	def getDescription( self): 
		"""
		 @description:string
		"""
		return self.description

	def setDescription( self, description): 
		"""
		 @description:string
		"""
		self.description=str(description)
		return True

	def getSecCode( self): 
		"""
		 @sec_code:bit(5)
		"""
		return self.sec_code

	def setSecCode( self, sec_code): 
		"""
		 @sec_code:bit(5)
		"""
		# 5bit + 2bit (0bxxxxx)
		"""
		if int(sec_code) <= 0b11111:
			self.sec_code=int(sec_code)
			return True
		else:
			return False
		"""
		self.sec_code=str(sec_code)
		return True

	def getDevCode( self):
		"""
		 @dev_code:int
		"""
		return self.dev_code

	def setDevCode( self, dev_code): 
		"""
		 @dev_code:int
		"""
		self.dev_code=int(dev_code)
		return True

	def getPlace( self): 
		"""
		 @place:string
		"""
		return self.place

	def setPlace( self, place): 
		"""
		 @place:string
		"""
		self.place=str(place)
		return True

	def getStatus( self): 
		"""
		 @status:boolean
		"""
		return self.status

	def setStatus( self, status): 
		"""
		 @status:boolean
		"""
		self.status=bool(status)
		return True

	def toggleStatus( self): 
		"""
		"""
		self.status=self.status^1
		return True

	def getBwlStatus( self): 
		"""
		 @bwl_status:bit(2)
		"""
		return self.bwl_status

	def setBwlStatus( self, bwl_status): 
		"""
		 @bwlListNr:bit(2)
		"""
		if int(bwl_status) <= 0b11:
			self.bwl_status=int(bwl_status)
			return True
		else:
			return False
	
	def getUser( self):
		"""
		 @id_user:int
		"""
		return self.id_user
	
	def getBlacklist( self): 
		"""
		 @id_blacklist:int
		"""
		return self.id_blacklist

	def setBlacklist( self, id_blacklist): 
		"""
		 @id_blacklist:int
		"""
		self.id_blacklist=int(id_blacklist)
		return True

	def removeBlacklist( self): 
		"""
		"""
		self.id_blacklist=""
		return True

	def getWhitelist( self): 
		"""
		 @id_whitelist:int
		"""
		return self.id_whitelist

	def setWhitelist( self, id_whitelist): 
		"""
		 @id_whitelist:int
		"""
		self.id_whitelist=int(id_whitelist)
		return True

	def removeWhitelist( self): 
		"""
		"""
		self.id_whitelist=""
		return True

	def getUndefinedbwllist( self): 
		"""
		 @id_undefinedbwllist:int
		"""
		return self.id_undefinedbwllist

	def setUndefinedbwllist( self, id_undefinedbwllist): 
		"""
		 @id_undefinedbwllist:int
		"""
		self.id_undefinedbwllist=int(id_undefinedbwllist)
		return True

	def removeUndefinedbwllist( self): 
		"""
		"""
		self.id_undefinedbwllist=""
		return True


