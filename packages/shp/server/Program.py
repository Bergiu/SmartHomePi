# 
from datetime import datetime
class Program (  ):
	#private:
	"""
	id # int
	datetime # datetime
	description # string
	status # boolean
	inactive # boolean
	weekly # boolean
	pin # int(4)
	id_related_dev # int
	id_initiator # int
	"""

	def __init__(self,**kwargs):
		"""
		 @**kwargs:
		  id: int
		  datetime: datetime
		  description: string = ""
		  status: boolean
		  inactive: boolean = False
		  weekly: boolean = False
		  pin: int(4) = ""
		  id_related_dev: int
		  id_initiator: int
		"""
		err_func="User __init__: "
		missing="User __init__: Missing "
		if "id" in kwargs.keys():
			self.id=int(kwargs["id"])
		else:
			raise ValueError(missing+"id")
		if "datetime" in kwargs.keys():
			if type(kwargs["datetime"]) is datetime:
				self.datetime=kwargs["datetime"]
			else:
				raise ValueError(err_func+"The key datetime is not propper type(datetime): "+type(kwargs["datetime"]))
		else:
			raise ValueError(missing+"datetime")
		if "description" in kwargs.keys():
			self.description=str(kwargs["description"])
		else:
			self.description=""
		if "status" in kwargs.keys():
			self.status=bool(kwargs["status"])
		else:
			raise ValueError(missing+"status")
		if "inactive" in kwargs.keys():
			self.inactive=bool(kwargs["inactive"])
		else:
			self.inactive=False
		if "weekly" in kwargs.keys():
			self.weekly=bool(kwargs["weekly"])
		else:
			self.weekly=False
		if "pin" in kwargs.keys():
			if int(kwargs["pin"]) >= 0 and int(kwargs["pin"]) < 10000:
				self.pin=int(kwargs["pin"])
			else:
				raise ValueError(err_func+"Pin must be between 0 and 10000")
		else:
			self.pin=""
		if "id_related_dev" in kwargs.keys():
			self.id_related_dev=int(kwargs["id_related_dev"])
		else:
			raise ValueError(missing+"id_related_dev")
		if "id_initiator" in kwargs.keys():
			self.id_initiator=int(kwargs["id_initiator"])
		else:
			raise ValueError(missing+"id_initiator")
		

	#public:
	def getId( self): 
		"""
		 @id:int
		"""
		return self.id

	def getDatetime( self): 
		"""
		 @datetime:datetime
		"""
		return self.datetime

	def setDatetime( self, date_time): 
		"""
		 @date_time:datetime
		"""
		if type(date_time) is datetime:
			self.datetime=date_time
			return True
		else:
			return False		

	def getDescription( self): 
		"""
		 @description:string
		"""
		return self.description

	def setDescription( self, description): 
		"""
		 @description:string
		"""
		self.description=str(description)
		return True

	def getStatus( self): 
		"""
		 @status:boolean
		"""
		return self.status

	def setStatus( self, status): 
		"""
		 @status:boolean
		"""
		self.status=bool(status)
		return True

	def toggleStatus( self): 
		"""
		"""
		# xor = ^
		self.status=bool(self.status^1)
		return True

	def isInactive( self): 
		"""
		 @inactive:boolean
		"""
		return self.inactive

	def setInactive( self, inactive): 
		"""
		 @inactive:boolean
		"""
		self.inactive=bool(inactive)
		return True

	def toggleInactive( self): 
		"""
		"""
		# xor = ^
		self.inactive=bool(self.inactive^1)
		return True

	def isWeekly( self): 
		"""
		 @weekly:boolean
		"""
		return self.weekly

	def setWeekly( self, weekly): 
		"""
		 @weekly:boolean
		"""
		self.weekly=bool(weekly)
		return True

	def toggleWeekly( self): 
		"""
		"""
		# xor = ^
		self.weekly=bool(self.weekly^1)
		return True

	def checkPin( self, pin): 
		"""
		 @pin:int(4)
		 @validPin:boolean :	
		"""
		if self.pin=="":
			return True
		elif type(pin) is str:
			return False
		elif self.pin == int(pin):
			return True
		else:
			return False

	def setPin( self, pin): 
		"""
		 @pin:int(4)
		"""
		if int(pin) >= 0 and int(pin) < 10000:
			self.pin=int(pin)
			return True
		else:
			return False
	
	def removePin( self):
		"""
		"""
		self.pin=""
		return True

	def getDevice( self): 
		"""
		 @device:Device
		"""
		return self.id_related_dev

	def getUser( self): 
		"""
		 @user:User
		"""
		return self.id_initiator



