# 
class Client (  ):
	#private:
	"""
	id # int
	ip_adress # string
	key # text
	place # string
	"""

	#public:
	def __init__(self, **kwargs):
		"""
		 @**kwargs:
		  id:int
		  ip_adress:string
		  key:string
		  place:string = ""
		"""
		missing="Server __init__: Missing "
		if "id" in kwargs.keys():
			self.id=int(kwargs["id"])
		else:
			raise ValueError(missing+"id")
		if "ip_adress" in kwargs.keys():
			self.ip_adress=str(kwargs["ip_adress"])
		else:
			raise ValueError(missing+"ip_adress")
		if "key" in kwargs.keys():
			self.key=str(kwargs["key"])
		else:
			raise ValueError(missing+"key")
		if "place" in kwargs.keys():
			self.place=str(kwargs["place"])
		else:
			self.place=""
	
	def getId( self): 
		"""
		 @id:int
		"""
		return self.id

	def getIpAdress( self): 
		"""
		 @ip_adress:string
		"""
		return self.ip_adress
	
	def setIpAdress( self, ip_adress):
		"""
		 @ip_adress:string
		"""
		self.ip_adress=str(ip_adress)

	def getPlace( self): 
		"""
		 @place:string
		"""
		return self.place
	
	def setPlace( self, place):
		"""
		 @place:string
		"""
		self.place=str(place)
		return True
	
	def getKey( self): 
		"""
		 @key:string
		"""
		return self.key
	
	def setKey( self, key):
		"""
		 @key:string
		"""
		self.key=str(key)
		return True


