# 
class ProgramController ( self,  ):
	#private:
	user_controller # UserController
	device_group_controller # DeviceGroupController
	device_controller # DeviceController


	#private:
	def getItemById( self, id_program,  program): 
		"""
		 @id_program:int
		 @program:Program
		"""
		return None


	#public:
	def getWeeklyPrograms( self, weekly_programs): 
		"""
		 @weekly_programs:array
		"""
		return None

	def getUniquePrograms( self, weekly_programs): 
		"""
		 @weekly_programs:array
		"""
		return None

	def getId( self, idx_program,  id): 
		"""
		 @idx_program:int
		 @id:int
		"""
		return None

	def getDatetime( self, id_program,  datetime): 
		"""
		 @id_program:int
		 @datetime:datetime
		"""
		return None

	def setDatetime( self, id_program,  datetime,  validation): 
		"""
		 @id_program:int
		 @datetime:datetime
		"""
		return None

	def getDescription( self, id_program,  description): 
		"""
		 @id_program:int
		 @description:string
		"""
		return None

	def setDescription( self, id_program,  description,  validation): 
		"""
		 @id_program:int
		 @description:string
		"""
		return None

	def getStatus( self, id_program,  status): 
		"""
		 @id_program:int
		 @status:boolean
		"""
		return None

	def setStatus( self, id_program,  status,  validation): 
		"""
		 @id_program:int
		 @status:boolean
		"""
		return None

	def toggleStatus( self, id_program,  validation): 
		"""
		 @id_program:int
		"""
		return None

	def isInactive( self, id_program,  inactive): 
		"""
		 @id_program:int
		 @inactive:boolean
		"""
		return None

	def setInactive( self, id_program,  inactive,  validation): 
		"""
		 @id_program:int
		 @inactive:boolean
		"""
		return None

	def toggleInactive( self, id_program,  validation): 
		"""
		 @id_program:int
		"""
		return None

	def isWeekly( self, id_program,  weekly): 
		"""
		 @id_program:int
		 @weekly:boolean
		"""
		return None

	def setWeekly( self, id_program,  weekly,  validation): 
		"""
		 @id_program:int
		 @weekly:boolean
		"""
		return None

	def toggleWeekly( self, id_program,  validation): 
		"""
		 @id_program:int
		"""
		return None

	def checkPin( self, id_program,  pin,  validPin): 
		"""
		 @id_program:int
		 @pin:int(4)
		 @validPin:boolean :	
		"""
		return None

	def setPin( self, id_program,  pin,  validation): 
		"""
		 @id_program:int
		 @pin:int(4)
		"""
		return None

	def getDevice( self, id_program,  device): 
		"""
		 @id_program:int
		 @device:Device
		"""
		return None

	def getUser( self, id_program,  user): 
		"""
		 @id_program:int
		 @user:User
		"""
		return None

	def getSendParams( self, id_program,  sec_code,  dev_code,  status): 
		"""
		 @id_program:int
		 @sec_code:bit(5)
		 @dev_code:int
		 @status:boolean
		"""
		return None

	def setUserController( self, user_controller,  validation): 
		"""
		 @user_controller:UserController
		"""
		return None

	def setDeviceGroupController( self, device_group_controller,  validation): 
		"""
		 @device_group_controller:DeviceGroupController
		"""
		return None

	def setDeviceController( self, device_controller,  validation): 
		"""
		 @device_controller:DeviceController
		"""
		return None


