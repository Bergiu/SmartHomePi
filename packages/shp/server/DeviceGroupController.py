# 
class DeviceGroupController ( self,  ):
	#private:
	device_controller # DeviceController


	#private:
	def getItemById( self, id_user,  device_group): 
		"""
		 @id_user:int
		 @device_group:DeviceGroup
		"""
		return None


	#public:
	def getPrivateGroups( self, private_groups): 
		"""
		 @private_groups:array
		"""
		return None

	def getGlobalGroups( self, global_groups): 
		"""
		 @global_groups:array
		"""
		return None

	def getId( self, idx_device_group,  id): 
		"""
		 @idx_device_group:int
		 @id:int
		"""
		return None

	def getDescription( self, id_device_group,  description): 
		"""
		 @id_device_group:int
		 @description:string
		"""
		return None

	def setDescription( self, id_device_group,  description,  validation): 
		"""
		 @id_device_group:int
		 @description:string
		"""
		return None

	def isPrivate( self, id_device_group,  private): 
		"""
		 @id_device_group:int
		 @private:boolean
		"""
		return None

	def getDevices( self, id_device_group,  devices): 
		"""
		 @id_device_group:int
		 @devices:array
		"""
		return None

	def addDevice( self, id_device_group,  id_device,  validation): 
		"""
		 @id_device_group:int
		 @id_device:int
		"""
		return None

	def removeDevice( self, id_device_group,  id_device,  validation): 
		"""
		 @id_device_group:int
		 @id_device:int
		"""
		return None

	def setDeviceController( self, device_controller,  validation): 
		"""
		 @device_controller:DeviceController
		"""
		return None


