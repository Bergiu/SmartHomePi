# 
from ..Controller import Controller
from .BwlListUser import BwlListUser
from .UserController import UserController
class BwlListUserController ( Controller ):
	#private:
	itemList = [] # Array
	indexIdx = 0 # int
	database="" #Database
	table_name="tbl_bwl_list_usr"
	
	def __init__( self, process, database, **kwargs):
		"""
		 @database: Database
		 @**kwargs
		  user_controller: UserRoleController
		"""
		super(BwlListUserController,self).__init__(process, database)
		if "user_controller" in kwargs.keys():
			self.user_controller=kwargs["user_controller"]
		else:
			self.user_controller=""
		#self.initiateAllFromDB()

	def delItem( self, id_item ): 
		"""
		 @id_item:int
		"""
		item=self.getItemById(id_item)
		if item:
			validation=self.database.delete(self.table_name,id_item)
			if not validation:
				return False
			self.itemList.remove(item)
			self.indexIdx-=1
			return True
		else:
			raise Exception("No BwlListUser with id=%s"%(item.getId()))
	
	def createItem(self,**kwargs):
		missing="BwlListUserController createBwlList: Missing "
		params={}
		if "id_user" in kwargs.keys():
			params["id_user"]=int(kwargs["id_user"])
		else:
			raise ValueError(missing+"id_user")
		if "id_bwl_list" in kwargs.keys():
			params["id_bwl_list"]=int(kwargs["id_bwl_list"])
		else:
			raise ValueError(missing+"id_bwl_list")
		params["id"]=self.database.insert(self.table_name,**params)
		item=BwlListUser(**params)
		validation=self.addItem(item)
		if validation:
			return item.getId()
		else:
			return False

	def initiateFromDB( self, id_item ): 
		"""
		 @id_item:int
		"""
		if self.user_controller:
			item_data=self.database.load(self.table_name,id_item)
			if item_data:
				params={}
				params["id"]=int(item_data[0])
				duplicate=self.isItem(id)
				if duplicate:
					self.removeItem(id)
				user_id=int(item_data[1])
				if self.user_controller.getItemById(int(user_id)):
					params["id_user"]=int(user_id)
				else:
					print("no user with id %s"%(user_id))
				params["id_bwl_list"]=int(item_data[2])
				item=BwlListUser(**params)
				self.addItem(item)
				return True
			else:
				return False
		else:
			return False

	def initiateAllFromDB( self ): 
		"""
		"""
		if self.user_controller:
			item_datas=self.database.loadTable(self.table_name)
			for item_data in item_datas:
				params={}
				params["id"]=int(item_data[0])
				duplicate=self.isItem(id)
				if duplicate:
					self.removeItem(id)
				user_id=int(item_data[1])
				if self.user_controller.getItemById(int(user_id)):
					params["id_user"]=int(user_id)
				else:
					print("no user with id %s"%(user_id))
				params["id_bwl_list"]=int(item_data[2])
				item=BwlListUser(**params)
				self.addItem(item)
			return True
		else:
			return False
	#end interface

	#public:
	def getIdByIdUserAndIdBwlList( self, id_user, id_bwl_list):
		validation=False
		for item in self.itemList:
			if item.getUser() == id_user and item.getBwlList() == id_bwl_list:
				validation=item.getId()
		return validation
	
	def getBwlList( self, id_bwl_list_user): 
		"""
		 @id_bwl_list:int
		 @ids_user:array
		"""
		item=self.getItemById(id_bwl_list_user)
		return item.getBwlList()
		
	def getUser( self, id_bwl_list_user): 
		"""
		 @id_bwl_list:int
		 @ids_user:array
		"""
		item=self.getItemById(id_bwl_list_user)
		return item.getUser()

	def setUserController( self, user_controller): 
		"""
		 @user_controller:UserController
		"""
		self.user_controller=user_controller
		return True


