# 
class DeviceGroup ( self,  ):
	#private:
	"""
	id # int
	description # string
	private # boolean
	ids_devices # array
	"""

	#public:
	def __init__( self, **kwargs):
		"""
		 @**kwargs:
		  id: int
		  description: string
		  private: boolean = False
		  ids_device: Array = []
		"""
		missing="HierarchyRole __init__: Missing "
		if "id" in kwargs.keys():
			self.id=int(kwargs["id"])
		else:
			raise ValueError(missing+"id")
		if "description" in kwargs.keys():
			self.description=str(kwargs["description"])
		else:
			raise ValueError(missing+"description")
		if "private" in kwargs.keys():
			self.private=bool(kwargs["private"])
		else:
			self.private=False
		if "ids_device" in kwargs.keys():
			self.ids_device=list(kwargs["ids_device"])
		else:
			self.ids_device=[]
	
	
	def getId( self): 
		"""
		 @id:int
		"""
		return self.id

	def getDescription( self): 
		"""
		 @description:string
		"""
		return self.description

	def setDescription( self, description): 
		"""
		 @description:string
		"""
		self.description=description
		return True

	def isPrivate( self): 
		"""
		 @private:boolean
		"""
		return self.private

	def getDevices( self): 
		"""
		 @devices:array
		"""
		return self.devices

	def addDevice( self, id_device): 
		"""
		 @id_device:int
		"""
		if id_device in self.ids_device:
			return False
		else:
			self.ids_device.append(int(id_device))
			return True

	def removeDevice( self, id_device,  validation): 
		"""
		 @id_device:int
		"""
		if int(id_device) in self.ids_device:
			self.ids_device.remove(ids_device)
			return True
		else:
			return False


