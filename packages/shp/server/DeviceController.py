# 
from ..bcolors import bcolors
from ..Controller import Controller
from .Device import Device
from .UserController import UserController
#from ProgramController import ProgramController
#Programme müssen gelöscht werden, wenn Device nicht mehr existiert
from .BwlListController import BwlListController
from .ClientController import ClientController
class DeviceController ( Controller ):
	#private:
	"""
	user_controller # UserController
	program_controller # ProgramController
	bwl_list_controller # BwlListController
	"""

	#private:
	table_name="tbl_dev"

	def __init__( self, process, database, **kwargs):
		"""
		 @database: Database
		 @**kwargs
		  user_controller: UserController
		  program_controller: ProgramController
		  bwl_list_controller: BwlListController
		"""
		super(DeviceController,self).__init__(process,database)
		if "user_controller" in kwargs.keys():
			self.user_controller=kwargs["user_controller"]
			self.bwl_list_controller=BwlListController(process,database,user_controller=kwargs["user_controller"])
		else:
			self.user_controller=""
			self.bwl_list_controller=BwlListController(process,database)
		if "program_controller" in kwargs.keys():
			self.program_controller=kwargs["program_controller"]
		else:
			self.program_controller=""
		if "client_controller" in kwargs.keys():
			self.client_controller=kwargs["client_controller"]
		else:
			self.client_controller=""
		#self.initiateAllFromDB()

	def createItem(self, session_id, **kwargs):
		"""
		 @session_id: int
		 @**kwargs:
		  description:string
		  sec_code:int
		  dev_code:int
		  place:string = ""
		"""
		err_func="DeviceController createItem: "
		missing="DeviceController createItem: Missing "
		perm="device.createDev"
		perm_id=10
		params_db={}
		params={}
		if not self.user_controller:
			raise Exception("You must set the user_controller to create new Devices.")
		if not self.user_controller.selfHasPermission(session_id, perm_id):
			raise Exception("No Permission to create Device.")
		if not self.bwl_list_controller:
			raise Exception("You must set the bwl_list_controller to create new Devices.")
		if "description" in kwargs.keys():
			params["description"]=str(kwargs["description"])
		else:
			raise ValueError(missing+"description")
		if "sec_code" in kwargs.keys():
			# 5bit + 2bit (0bxxxxx)
			if len(str(kwargs["sec_code"])) <= 5:
				validation = True
				for i in str(kwargs["sec_code"]):
					if not (i == "1" or i == "0"):
						validation=False
				if not validation:
					raise ValueError(err_func+"sec_code must be 5 Bit binary")
				params["sec_code"]=str(kwargs["sec_code"])
			else:
				raise ValueError(err_func+"sec_code must be 5 Bit binary")
			"""
			#alt, ohne validations prüfung
			params["sec_code"]=str(kwargs["sec_code"])
			params_db["sec_code"]=str(kwargs["sec_code"])
			"""
		else:
			raise ValueError(missing+"sec_code")
		if "dev_code" in kwargs.keys():
			params["dev_code"]=int(kwargs["dev_code"])
		else:
			raise ValueError(missing+"dev_code")
		if "place" in kwargs.keys():
			params["place"]=str(kwargs["place"])
		else:
			params["place"]=""
		
		for device in self.getAllItems():
			if device.getSecCode() == params["sec_code"] and device.getDevCode() == params["dev_code"]:
				raise Exception("Dieses Gerät existiert bereits unter dem Namen: %s"%(device.getDescription()))
		
		params["id_user"]=int(self.user_controller.getEingeloggterUser(session_id))
		params["bwl_status"]=0
		params["status"]=bool(0)
		
		params["id"]=int(self.database.insert(self.table_name,**params))
		
		params["id_blacklist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=1)
		params["id_whitelist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=2)
		params["id_undefinedbwllist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=3)
		
		item=Device(**params)
		validation=self.addItem(item)
		if validation:
			return item.getId()
		else:
			return False

	def delItem( self, session_id, id_item ): 
		"""
		 @id_item:int
		"""
		perm="device.deleteDev"
		perm_id=13
		item=self.getItemById(id_item)
		if item:
			if self.user_controller:
				id_user=item.getUser()
				id_hierarchy_lvl=self.user_controller.getHierarchyLvl(id_user)
				id_eingeloggter_user=self.user_controller.getEingeloggterUser(session_id)
				if self.user_controller.selfHasPermissionOn(session_id, perm_id, id_hierarchy_lvl) or id_eingeloggter_user == id_user:
					validation=self.database.delete(self.table_name,id_item)
					if not validation:
						return False
					self.itemList.remove(item)
					self.indexIdx-=1
					return True
				else:
					raise Exception("No Permission to delete Device %s (id=%s)"%(item.getDescription(),item.getId()))
			else:
				raise Exception("You must set the user_controller to delete Devices")
		else:
			raise Exception("No Device with id=%s"%(id_item))
	
	def initiateFromDB( self, id_item ): 
		"""
		 @id_item:int
		 @validation:boolean
		"""
		if self.bwl_list_controller and self.user_controller:
			item_data=self.database.load(self.table_name,id_item)
			if item_data:
				params={}
				params["id"]=int(item_data[0])
				duplicate=self.isItem(params["id"])
				if duplicate:
					self.removeItem(params["id"])
				params["sec_code"]=str(item_data[1])
				params["dev_code"]=int(item_data[2])
				params["description"]=str(item_data[3])
				params["place"]=str(item_data[4])
				params["status"]=bool(item_data[5])
				if item_data[6] == None:
					params["bwl_status"]=0
				else:
					params["bwl_status"]=int(item_data[6])
				params["id_user"]=int(item_data[7])
				if not self.user_controller.getItemById(params["id_user"]):
					print("Warning: User not found: ID=%s on Device id=%s "%(params["id_user"],params["id"]))
					
				table_name_bwl_list=self.bwl_list_controller.getTableName()
				ids_bwl_list=self.database.loadForeignKeys(table_name_bwl_list,"id_device",self.table_name,params["id"])
				params["id_blacklist"]=""
				params["id_whitelist"]=""
				params["id_undefinedbwllist"]=""
				for id_bwl_list in ids_bwl_list:
					bwl_status=self.bwl_list_controller.getBwlStatus(id_bwl_list)
					if bwl_status == 1:
						params["id_blacklist"]=id_bwl_list
					elif bwl_status == 2:
						params["id_whitelist"]=id_bwl_list
					elif bwl_status == 3:
						params["id_undefinedbwllist"]=id_bwl_list
				if not params["id_blacklist"]:
					params["id_blacklist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=1)
				if not params["id_whitelist"]:
					params["id_whitelist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=2)
				if not params["id_undefinedbwllist"]:
					params["id_undefinedbwllist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=3)
					
				item=Device(**params)
				self.addItem(item)
				return True
			else:
				return False
		else:
			return False

	def initiateAllFromDB( self ): 
		"""
		 @validation:boolean
		"""
		if self.bwl_list_controller and self.user_controller:
			item_datas=self.database.loadTable(self.table_name)
			for item_data in item_datas:
				params={}
				params["id"]=int(item_data[0])
				duplicate=self.isItem(params["id"])
				if duplicate:
					self.removeItem(params["id"])
				params["sec_code"]=str(item_data[1])
				params["dev_code"]=int(item_data[2])
				params["description"]=str(item_data[3])
				params["place"]=str(item_data[4])
				params["status"]=bool(item_data[5])
				if item_data[6] == None:
					params["bwl_status"]=0
				else:
					params["bwl_status"]=int(item_data[6])
				params["id_user"]=int(item_data[7])
				if not self.user_controller.getItemById(params["id_user"]):
					print("Warning: User not found: ID=%s on Device id=%s "%(params["id_user"],params["id"]))
				
				table_name_bwl_list=self.bwl_list_controller.getTableName()
				ids_bwl_list=self.database.loadForeignKeys(table_name_bwl_list,"id_device",self.table_name,params["id"])
				params["id_blacklist"]=""
				params["id_whitelist"]=""
				params["id_undefinedbwllist"]=""
				for id_bwl_list in ids_bwl_list:
					bwl_status=self.bwl_list_controller.getBwlStatus(id_bwl_list)
					if bwl_status == 1:
						params["id_blacklist"]=id_bwl_list
					elif bwl_status == 2:
						params["id_whitelist"]=id_bwl_list
					elif bwl_status == 3:
						params["id_undefinedbwllist"]=id_bwl_list
				if not params["id_blacklist"]:
					params["id_blacklist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=1)
				if not params["id_whitelist"]:
					params["id_whitelist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=2)
				if not params["id_undefinedbwllist"]:
					params["id_undefinedbwllist"]=self.bwl_list_controller.createItem(id_device=params["id"],bwl_status=3)
				item=Device(**params)
				self.addItem(item)
			return True
		else:
			return False
	#end interface

	#public:

	def getDescription( self, id_device): 
		"""
		 @id_device:int
		 @description:string
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getDescription()
		else:
			return False

	def getUser(self, id_device):
		"""
		
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getUser()
		else:
			return False

	def setDescription( self, session_id, id_device,  description): 
		"""
		 @id_device:int
		 @description:string
		 @validation:boolean
		"""
		perm="device.changeDevData"
		perm_id=12
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		item=self.getItemById(id_device)
		if not item:
			raise Exception("No Device with id=%s"%(id_device))
		id_user=item.getUser()
		id_hierarchy_lvl=self.user_controller.getHierarchyLvl(id_user)
		id_eingeloggter_user=self.user_controller.getEingeloggterUser(session_id)
		if self.user_controller.selfHasPermissionOn(session_id, perm_id, id_hierarchy_lvl) or id_eingeloggter_user == id_user:
				validation=item.setDescription(str(description))
				if validation:
					validation=self.database.update(self.table_name,id_device,description=str(description))
					return validation
				else:
					return False
		else:
			raise Exception("No Permission to change Device Data.")

	def getSecCode( self, id_device): 
		"""
		 @id_device:int
		 @sec_code:int
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getSecCode()
		else:
			return False

	def setSecCode( self, session_id, id_device,  sec_code): 
		"""
		 @id_device:int
		 @sec_code:bit(5)
		 @validation:boolean
		"""
		perm="device.changeDevData"
		perm_id=12
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		item=self.getItemById(id_device)
		if not item:
			raise Exception("No Device with id=%s"%(id_device))
		id_user=item.getUser()
		id_hierarchy_lvl=self.user_controller.getHierarchyLvl(id_user)
		id_eingeloggter_user=self.user_controller.getEingeloggterUser(session_id)
		if self.user_controller.selfHasPermissionOn(session_id, perm_id, id_hierarchy_lvl) or id_eingeloggter_user == id_user:
				validation=item.setSecCode(str(sec_code))
				if validation:
					validation=self.database.update(self.table_name,id_device,sec_code=str(sec_code))
					return validation
				else:
					return False
		else:
			raise Exception("No Permission to change Device Data.")

	def getDevCode( self, id_device): 
		"""
		 @id_device:int
		 @dev_code:int
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getDevCode()
		else:
			return False

	def setDevCode( self, session_id, id_device,  dev_code): 
		"""
		 @id_device:int
		 @dev_code:int
		 @validation:boolean
		"""
		perm="device.changeDevData"
		perm_id=12
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		item=self.getItemById(id_device)
		if not item:
			raise Exception("No Device with id=%s"%(id_device))
		id_user=item.getUser()
		id_hierarchy_lvl=self.user_controller.getHierarchyLvl(id_user)
		id_eingeloggter_user=self.user_controller.getEingeloggterUser(session_id)
		if self.user_controller.selfHasPermissionOn(session_id, perm_id, id_hierarchy_lvl) or id_eingeloggter_user == id_user:
				validation=item.setDevCode(str(dev_code))
				if validation:
					validation=self.database.update(self.table_name,id_device,dev_code=int(dev_code))
					return validation
				else:
					return False
		else:
			raise Exception("No Permission to change Device Data.")


	def getPlace( self, id_device): 
		"""
		 @id_device:int
		 @place:string
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getPlace()
		else:
			return False

	def setPlace( self, session_id, id_device,  place): 
		"""
		 @id_device:int
		 @place:string
		 @validation:boolean
		"""
		perm="device.changeDevData"
		perm_id=12
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		item=self.getItemById(id_device)
		if not item:
			raise Exception("No Device with id=%s"%(id_item))
		id_user=item.getUser()
		id_hierarchy_lvl=self.user_controller.getHierarchyLvl(id_user)
		id_eingeloggter_user=self.user_controller.getEingeloggterUser(session_id)
		if self.user_controller.selfHasPermissionOn(session_id, perm_id, id_hierarchy_lvl) or id_eingeloggter_user == id_user:
				validation=item.setPlace(str(place))
				if validation:
					validation=self.database.update(self.table_name,id_device,place=str(place))
					return validation
				else:
					return False
		else:
			raise Exception("No Permission to change Device Data.")

	def isAllowedToChageStatus( self, session_id, id_device):
		"""
		 User is allowed if:
		 
		  - there is no bwl list chosen and the user has the permission:
		    "device.changeDevStatus" (id=11)
		  - there is no bwl list chosen and the user is the initiator of this device
		  
		  - he is on the whitelist,
		  
		  - he is not on the blacklist, and has the permission:
		    "device.changeDevStatus" (id=11)
		  - he is not on the blacklist, and is the initiator of this device
		  
		 else the User is NOT allowed if:
		  - the bwl status is 3
		"""
		def hasNormalPermission():
			perm="device.changeDevStatus"
			perm_id=11
			id_owner=item.getUser()
			id_hierarchy_lvl=self.user_controller.getHierarchyLvl(id_owner)
			id_eingeloggter_user=self.user_controller.getEingeloggterUser(session_id)
			is_owner=(id_eingeloggter_user == id_owner)
		
			has_permission=self.user_controller.selfHasPermissionOn(session_id, perm_id, id_hierarchy_lvl)
			return is_owner or has_permission
		
		item=self.getItemById(id_device)
		if not item:
			raise Exception("No Device with id=%s"%(id_item))
		if not self.bwl_list_controller:
			raise Exception("You must set the bwl_list_controller.")
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		bwl_status=self.getBwlStatus(id_device)
		id_user=self.user_controller.getEingeloggterUser(session_id)
		if id_user==item.getUser():
			return True
		if bwl_status == 0:
			#print("no list")
			if hasNormalPermission():
				return True
			else:
				return False
		elif bwl_status == 1:
			#print("blacklist")
			id_bwl_list=self.getBlacklist(id_device)
			is_listed=self.bwl_list_controller.isUserListed(id_bwl_list, id_user)
			if is_listed:
				return False
			else:
				if hasNormalPermission():
					return True
				else:
					return False
		elif bwl_status == 2:
			#print("whitelist")
			id_bwl_list=self.getWhitelist(id_device)
			is_listed=self.bwl_list_controller.isUserListed(id_bwl_list, id_user)
			if is_listed:
				return True
			else:
				return False
		elif bwl_status == 3:
			#print("undefinedlist")
			#I don't know what to do here, because this is not defined
			#Maybe sometimes in near future this list gets some logic and is not so useless
			#Maybe not
			#(permissions depending on time)
			return False
		else:
			raise ValueError("Something went wrong")
			
	def getStatus( self, id_device): 
		"""
		 @id_device:int
		 @status:boolean
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getStatus()
		else:
			return False

	def setStatus( self, session_id, id_device, status): 
		"""
		 @id_device:int
		 @status:boolean
		 @validation:boolean
		"""
		item=self.getItemById(id_device)
		if not self.client_controller:
			raise Exception("You must set the client_controller.")
		if self.isAllowedToChageStatus(session_id, id_device):
			validation=item.setStatus(bool(status))
			if validation:
				validation=self.database.update(self.table_name,id_device,status=bool(status))
				if validation:
					validation=self.client_controller.sendStatusToAll(item.getSecCode(),item.getDevCode(),bool(status))
				return validation
			else:
				raise Exception("Something went wrong!")
		else:
			raise Exception("No Permission to change Device Status.")

	def toggleStatus( self, session_id, id_device): 
		"""
		 @id_device:int
		 @validation:boolean
		"""
		status=self.getStatus(id_device)
		if status:
			validation=self.setStatus(session_id,id_device,0)
			print("Turning device off")
		else:
			validation=self.setStatus(session_id,id_device,1)
			print("Turning device on")
		return validation

	def getBwlStatus( self, id_device): 
		"""
		 @id_device:int
		 @bwl_status:int
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getBwlStatus()
		else:
			return False

	def setBwlStatus( self, session_id, id_device,  bwl_status): 
		"""
		 @id_device:int
		 @bwl_status:int
		 @validation:boolean
		"""
		perm="device.manageBwl"
		perm_id=14
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		item=self.getItemById(id_device)
		if not item:
			raise Exception("No Device with id=%s"%(id_device))
		id_user=item.getUser()
		id_hierarchy_lvl=self.user_controller.getHierarchyLvl(id_user)
		id_eingeloggter_user=self.user_controller.getEingeloggterUser(session_id)
		if self.user_controller.selfHasPermissionOn(session_id, perm_id, id_hierarchy_lvl) or id_eingeloggter_user == id_user:
				validation=item.setBwlStatus(int(bwl_status))
				if validation:
					validation=self.database.update(self.table_name,id_device,bwl_status=int(bwl_status))
					return validation
				else:
					return False
		else:
			raise Exception("No Permission to change Device Data.")

	def isAllowedToManageBwlOn(self, session_id, id_device, id_user):
		try:
			validation=self.isAllowedToManageBwl(session_id,id_device)
		except Exception as e:
			raise e
		if not validation:
			return False
		#if you want to block the initiator
		user=self.user_controller.getItemById(id_user)
		if not user:
			raise Exception("There is no User with ID=%s"%(id_user))
		initiator=self.getUser(id_device)
		if initiator == id_user:
			raise Exception("You can't block the initiator of this device, that's mean")
		return True

	def isAllowedToManageBwl(self,session_id,id_device):
		perm="device.manageBwl"
		perm_id=14
		if not self.user_controller:
			raise Exception("You must set the user_controller.")
		if not self.bwl_list_controller:
			raise Exception("You must set the bwl_list_controller.")
		device=self.getItemById(id_device)
		if not device:
			raise Exception("There is no device with ID=%s"%(id_device))
		hlvl=self.user_controller.getHierarchyLvl(self.getUser(id_device))
		initiator=self.getUser(id_device)
		are_you_initiator= bool( initiator == self.user_controller.getEingeloggterUser(session_id) )
		#if you have permission, or are the initiator of this device
		if not self.user_controller.selfHasPermissionOn(session_id,perm_id,hlvl) and not are_you_initiator:
			return False
		return True

	def getBlacklist( self, id_device): 
		"""
		 @id_device:int
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getBlacklist()
		else:
			return False
		
	def getWhitelist( self, id_device): 
		"""
		 @id_device:int
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getWhitelist()
		else:
			return False

	def getUndefinedbwllist( self, id_device): 
		"""
		 @id_device:int
		"""
		item=self.getItemById(id_device)
		if item:
			return item.getUndefinedbwllist()
		else:
			return False
	
	def getBlacklistedUsers(self,session_id,id_device):
		"""
		 @session_id:str
		 @id_device:int
		"""
		try:
			self.isAllowedToManageBwl(session_id,id_device)
		except Exception as e:
			raise e
		id_bwl_list=self.getBlacklist(id_device)
		return self.bwl_list_controller.getListedUsers(id_bwl_list)
	
	def getWhitelistedUsers(self,session_id,id_device):
		"""
		 @session_id:str
		 @id_device:int
		"""
		try:
			self.isAllowedToManageBwl(session_id,id_device)
		except Exception as e:
			raise e
		id_bwl_list=self.getWhitelist(id_device)
		return self.bwl_list_controller.getListedUsers(id_bwl_list)
		
	def getUndefinedlistedUsers(self,session_id,id_device):
		"""
		 @session_id:str
		 @id_device:int
		"""
		try:
			self.isAllowedToManageBwl(session_id,id_device)
		except Exception as e:
			raise e
		id_bwl_list=self.getUndefinedlist(id_device)
		return self.bwl_list_controller.getListedUsers(id_bwl_list)
	
	def addUserToBwlList(self, session_id, id_device, id_user, bwl_status):
		try :
			self.isAllowedToManageBwlOn(session_id,id_device,id_user)
		except Exception as e:
			raise e
		
		if bwl_status == 1:
			id_bwl_list=self.getBlacklist(id_device)
			validation=self.bwl_list_controller.addUserToList(id_bwl_list,id_user)
		elif bwl_status == 2:
			id_bwl_list=self.getWhitelist(id_device)
			validation=self.bwl_list_controller.addUserToList(id_bwl_list,id_user)
		elif bwl_status == 3:
			id_bwl_list=self.getUndefined(id_device)
			validation=self.bwl_list_controller.addUserToList(id_bwl_list,id_user)
		return validation

	def addUserToBlacklist( self, session_id, id_device, id_user):
		if not self.getItemById(id_device):
			raise Exception("There is no device with ID=%s"%(id_device))
		return self.addUserToBwlList(session_id,id_device,id_user,1)
	
	def addUserToWhitelist( self, session_id, id_device, id_user):
		if not self.getItemById(id_device):
			raise Exception("There is no device with ID=%s"%(id_device))
		return self.addUserToBwlList(session_id,id_device,id_user,2)
		
	def addUserToUndefinedlist( self, session_id, id_device, id_user):
		if not self.getItemById(id_device):
			raise Exception("There is no device with ID=%s"%(id_device))
		return self.addUserToBwlList(session_id,id_device,id_user,3)
	
	def removeUserFromBwlList(self, session_id, id_device, id_user, bwl_status):
		try :
			self.isAllowedToManageBwlOn(session_id,id_device,id_user)
		except Exception as e:
			raise e
		if bwl_status == 1:
			id_bwl_list=self.getBlacklist(id_device)
			validation=self.bwl_list_controller.removeUserFromList(id_bwl_list,id_user)
		elif bwl_status == 2:
			id_bwl_list=self.getWhitelist(id_device)
			validation=self.bwl_list_controller.removeUserFromList(id_bwl_list,id_user)
		elif bwl_status == 3:
			id_bwl_list=self.getUndefined(id_device)
			validation=self.bwl_list_controller.removeUserFromList(id_bwl_list,id_user)
		return validation

	def removeUserFromBlacklist( self, session_id, id_device, id_user):
		if not self.getItemById(id_device):
			raise Exception("There is no device with ID=%s"%(id_device))
		return self.removeUserFromBwlList(session_id,id_device,id_user,1)
	
	def removeUserFromWhitelist( self, session_id, id_device, id_user):
		if not self.getItemById(id_device):
			raise Exception("There is no device with ID=%s"%(id_device))
		return self.removeUserFromBwlList(session_id,id_device,id_user,2)
		
	def removeUserFromUndefinedlist( self, session_id, id_device, id_user):
		if not self.getItemById(id_device):
			raise Exception("There is no device with ID=%s"%(id_device))
		return self.removeUserFromBwlList(session_id,id_device,id_user,3)
	
	def setBwlListController( self, bwl_list_controller): 
		"""
		 @bwl_list_controller:BwlListController
		 @validation:boolean
		"""
		self.bwl_list_controller=bwl_list_controller
		return True

	def setProgramController( self, program_controller): 
		"""
		 @program_controller:ProgramController
		 @validation:boolean
		"""
		self.program_controller=program_controller
		return True

	def setUserController( self, user_controller): 
		"""
		 @user_controller:UserController
		 @validation:boolean
		"""
		self.user_controller=user_controller
		return True
		
	def setClientController( self, client_controller):
		"""
		 @client_controller:ClientController
		 @validation:boolean
		"""
		self.client_controller=client_controller
		return True

	def listDevices(self):
		#self.initiateAllFromDB()
		for d in self.getAllItems():
			if d.getStatus():
				status="On"
				color=bcolors.OKGREEN
			else:
				status="Off"
				color=bcolors.FAIL
			liste=""
			if d.getBwlStatus() == 0:
				liste="%sno bwl%s"%(bcolors.OKBLUE,bcolors.ENDC)
			if d.getBwlStatus() == 1:
				liste="%sblacklist%s"%(bcolors.WARNING,bcolors.ENDC)
			if d.getBwlStatus() == 2:
				liste="whitelist"
			if d.getBwlStatus() == 3:
				liste="%sundefinedlist%s"%(bcolors.FAIL,bcolors.ENDC)
			print("%s - %s (%s) [%s%s%s] [%s]"%(d.getId(),d.getDescription(),self.user_controller.getUsername(d.getUser()),color,status,bcolors.ENDC,liste))
