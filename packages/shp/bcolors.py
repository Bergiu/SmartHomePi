class bcolors:
	HEADER = '\033[95m'
	#lila
	
	OKBLUE = '\033[94m'
	#blau
	
	OKGREEN = '\033[92m'
	#grün
	
	WARNING = '\033[93m'
	#orange
	
	FAIL = '\033[91m'
	#red
	
	ENDC = '\033[0m'
	#normal
	
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	
	colors={
		"blue":OKBLUE,
		"green":OKGREEN,
		"purple":HEADER,
		"orange":WARNING,
		"red":FAIL
	}
