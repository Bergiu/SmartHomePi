# SmartHomePi

## 1. Unsere Idee:

Viele Geräte besitzen heut zu Tage eine „Smart“ Funktion und lassen sich per
App oder Fernbedienung ausschalten. Ältere Geräte besitzen diese Neuerung nicht,
obwohl man es sich bei vielen Geräten wünschen würde. Unser Gedankengang sah
so aus das die alten Geräte per Website und später auch per App ein bzw.
ausschaltbar sind. Voraussetzung hierfür ist lediglich das die Geräte sich einschalten,
sobald sie mit Strom versorgt werden. Geräte die bei der Versorgung mit Strom nur
auf „Standby“ fahren sind von unserer Verbesserung leider (noch) ausgeschlossen.

## 2. Vorraussetzungen:

1. RaspberryPi
2. einen 433MHz Sender (z.B: [433MHz Sender](https://www.amazon.de/receiver-Superregeneration-Wireless-Transmitter-Burglar/dp/B00ATZV5EQ))
3. 433MHz Funksteckdosen

## 3. Installation:
1. Auf dem Raspberry Pi z.B. Raspbian installieren
2. Folgende Programme installieren:
	* Apache2
	* MySQL
	* PHP5
	* PHP5-MySQL
	* MySQL-Server
	* Python3 (Ver. >= 3.3)
	* python3-pip
	* Flask
	* [wiringPi](https://github.com/WiringPi/WiringPi)
	* [raspberry-remote](https://github.com/xkonni/raspberry-remote)

	- LAMP Server:

		```shell
		sudo apt-get install apache2 libapache2-mod-php5 php5 php5-mysql mysql-server python3 python3-pip
		sudo pip3 install Flask
		sudo pip3 install PyMySql
		```

	- Dann (wiringPi)[https://github.com/WiringPi/WiringPi] 
		__Achten Sie darauf, dass Sie auch ohne Root, Rechte haben, die GPIO Pinne zu nutzen.__

		```shell
		sudo apt-get install wiringpi
		```


	- Und zum Schluss (raspberry-remote)[https://github.com/xkonni/raspberry-remote]
	__Achten Sie darauf, dass Sie auch ohne Root, Rechte haben, die GPIO Pinne zu nutzen.__

3. Der Sender wird an den Raspberry Pi angeschlossen

**433MHz Sender:**

* Pin 1: ATAD (Daten)
* Pin 2: VCC (Versorgungsspannung)
* Pin 3: GND (Masse)
* Pin 4: Ant (Antenne)

**GPIO:**

![GPIO Pinbelegung](img/gpio.png)

**Folgende Belegung:**

* Pin 1 (ATAD) an Pin 11 (GPIO17)(Gelb)
* Pin 2 (VCC) an Pin 2 oder 4 (5V)(Rot)
* Pin 3 (GND) an Pin 6 (GND)
* Pin 4 (Ant) an irgendein Kabel, als Antenne löten (oder z.B. eine Kugelschreiberfeder)
    
4. Jetzt können Sie unser Programm herunterladen und das setup.py Script als Root ausführen (2 mal, einmal für den Server und einmal für den Client). Geben Sie ihre MySQL Userdaten und danach einen Nutzernamen und ein Passwort für den Administrator der SmartHomePi Web-Oberfläche ein.  

	```shell
	sudo python3 setup.py
	```

5. Fertig! SmartHomePi wurde jetzt in */opt/python/* installiert. Um es zu starten geben Sie ein:

	Server:  
	```shell
	python3 /opt/python/shp_server/
	```

	und den Client:  
	```shell
	python3 /opt/python/shp_client/
	```

6. Sie erreichen die Webseite über den RaspberryPi auf dem Port 5522 (z.B.: *0.0.0.0:5522*)

## 4. Einrichtung:

1. Schließen Sie ihr Gerät (z.B. die Schreibtischlampe) an eine Funksteckdose an und merken sich die Pinbelegung der Funksteckdose.
2. Loggen Sie sich auf der Webseite mit dem Admin Account ein.
3. Klicken Sie auf Gerät hinzufügen und geben Sie die Daten der Pinbelegung wie beschrieben ein.
4. Fertig! Mit einem Klick auf Ein/Ausschalten können Sie ihr Gerät nun ganz bequem vom Pc aus Steuern.
